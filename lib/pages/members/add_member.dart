import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../home_page.dart';

class AddMember extends StatefulWidget {
  @override
  _AddMemberState createState() => _AddMemberState();
}

class _AddMemberState extends State<AddMember> {
  TextEditingController memberName = new TextEditingController();
  TextEditingController address = new TextEditingController();
  TextEditingController cluster = new TextEditingController();
  TextEditingController mobileNumber = new TextEditingController();
  TextEditingController nicNumber = new TextEditingController();
  TextEditingController loanAmount = new TextEditingController();
  TextEditingController loanTerm = new TextEditingController();
  TextEditingController interest = new TextEditingController();
  TextEditingController gur1Name = new TextEditingController();
  TextEditingController gur1Mobile = new TextEditingController();
  TextEditingController gur1Address = new TextEditingController();
  TextEditingController gur2Name = new TextEditingController();
  TextEditingController gur2Mobile = new TextEditingController();
  TextEditingController gur2Address = new TextEditingController();
  TextEditingController gur3Name = new TextEditingController();
  TextEditingController gur3Mobile = new TextEditingController();
  TextEditingController gur3Address = new TextEditingController();

  GlobalKey<AutoCompleteTextFieldState<String>> keyMemberName = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyAddress = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyCluster = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyGur1Name = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyGur1Mobile = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyGur1Address =
  new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyGur2Name = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyGur2Mobile = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyGur2Address =
  new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyGur3Name = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyGur3Mobile = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyGur3Address =
  new GlobalKey();

  final databaseReference = FirebaseFirestore.instance;

  ProgressDialog pr;

  @override
  Widget build(BuildContext context) {

    pr = ProgressDialog(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Add New Member"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Personal Details",
                  style: TextStyle(color: Colors.blue[200]),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.get("name"));
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyMemberName,
                      decoration: InputDecoration(
                          labelText: "Member Name",
                          hintText: "Member Name",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: memberName,
                      suggestions: suggessions,
                      clearOnSubmit: true,
                      textCapitalization: TextCapitalization.words,
                    );
                  } else {
                    return TextField(
                      controller: memberName,
                      decoration: InputDecoration(
                          labelText: "Member Name",
                          hintText: "Member Name",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      textCapitalization: TextCapitalization.words,
                    );
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.get("address"));
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyAddress,
                      decoration: InputDecoration(
                          labelText: "Address",
                          hintText: "Address",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: address,
                      suggestions: suggessions,
                      clearOnSubmit: true,
                      textCapitalization: TextCapitalization.words,
                    );
                  } else {
                    return TextField(
                      controller: address,
                      decoration: InputDecoration(
                          labelText: "Address",
                          hintText: "Address",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      textCapitalization: TextCapitalization.words,
                    );
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
            FutureBuilder<QuerySnapshot>(
              future: databaseReference.collection("clusters").get(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {

                  List<String> suggessions = [];

                  snapshot.data.docs.forEach((element) {
                    suggessions.add(element.get("name"));
                  });

                  return SimpleAutoCompleteTextField(
                    key: keyCluster,
                    decoration: InputDecoration(
                        labelText: "Cluster",
                        hintText: "Cluster",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                    controller: cluster,
                    suggestions: suggessions,
                    clearOnSubmit: true,
                    textCapitalization: TextCapitalization.words,
                  );
                } else {
                  return TextField(
                    controller: cluster,
                    decoration: InputDecoration(
                        labelText: "Cluster",
                        hintText: "Cluster",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                    textCapitalization: TextCapitalization.words,
                  );
                }
              },
            ),
              SizedBox(
                height: 10,
              ),
              TextField(
                controller: mobileNumber,
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                    labelText: "Mobile Number",
                    hintText: "Mobile Number",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5))),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                controller: nicNumber,
                decoration: InputDecoration(
                    labelText: "NIC Number",
                    hintText: "NIC Number",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5))),
              ),
              SizedBox(
                height: 20,
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Loan Details",
                  style: TextStyle(color: Colors.blue[200]),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                controller: loanAmount,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: "Loan Amount",
                    hintText: "Loan Amount",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5))),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                controller: loanTerm,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: "Loan Term",
                    hintText: "Loan Term",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5))),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                controller: interest,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: "Interest",
                    hintText: "Interest",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5))),
              ),
              SizedBox(
                height: 20,
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Guarenters Details",
                  style: TextStyle(color: Colors.blue[200]),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.data()["gur_1_name"] ?? '');
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyGur1Name,
                      decoration: InputDecoration(
                          labelText: "Guarenter 1 Name",
                          hintText: "Guarenter 1 Name",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: gur1Name,
                      suggestions: suggessions,
                      clearOnSubmit: false,
                      textCapitalization: TextCapitalization.words,
                      textSubmitted: (text) {
                        pr.show();
                        databaseReference.collection("members")
                            .where("gur_1_name", isEqualTo: text)
                            .get().then((value) {
                          pr.hide();
                          setState(() {
                            gur1Mobile.text = value.docs[0].data()["gur_1_mobile"]
                                .toString();
                            gur1Address.text = value.docs[0].data()["gur_1_address"]
                                .toString();
                          });
                        });
                      },
                    );
                  } else {
                    return TextField(
                      controller: gur1Name,
                      decoration: InputDecoration(
                          labelText: "Guarenter 1 Name",
                          hintText: "Guarenter 1 Name",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      textCapitalization: TextCapitalization.words,
                    );
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.data()["gur_1_mobile"] ?? '');
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyGur1Mobile,
                      decoration: InputDecoration(
                          labelText: "Guarenter 1 Mobile",
                          hintText: "Guarenter 1 Mobile",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: gur1Mobile,
                      suggestions: suggessions,
                      clearOnSubmit: true,
                      keyboardType: TextInputType.number,
                    );
                  } else {
                    return TextField(
                      controller: gur1Mobile,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          labelText: "Guarenter 1 Mobile",
                          hintText: "Guarenter 1 Mobile",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                    );
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.data()["gur_1_address"] ?? '');
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyGur1Address,
                      decoration: InputDecoration(
                          labelText: "Guarenter 1 Address",
                          hintText: "Guarenter 1 Address",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: gur1Address,
                      suggestions: suggessions,
                      clearOnSubmit: true,
                      textCapitalization: TextCapitalization.words,
                    );
                  } else {
                    return TextField(
                      controller: gur1Address,
                      decoration: InputDecoration(
                          labelText: "Guarenter 1 Address",
                          hintText: "Guarenter 1 Address",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      textCapitalization: TextCapitalization.words,
                    );
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.data()["gur_2_name"] ?? '');
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyGur2Name,
                      decoration: InputDecoration(
                          labelText: "Guarenter 2 Name",
                          hintText: "Guarenter 2 Name",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: gur2Name,
                      suggestions: suggessions,
                      clearOnSubmit: false,
                      textCapitalization: TextCapitalization.words,
                      textSubmitted: (text) {
                        pr.show();
                        databaseReference.collection("members")
                            .where("gur_2_name", isEqualTo: text)
                            .get().then((value) {
                          pr.hide();
                          setState(() {
                            gur2Mobile.text = value.docs[0].data()["gur_2_mobile"]
                                .toString();
                            gur2Address.text = value.docs[0].data()["gur_2_address"]
                                .toString();
                          });
                        });
                      },
                    );
                  } else {
                    return TextField(
                      controller: gur2Name,
                      decoration: InputDecoration(
                          labelText: "Guarenter 2 Name",
                          hintText: "Guarenter 2 Name",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      textCapitalization: TextCapitalization.words,
                    );
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.data()["gur_2_mobile"] ?? '');
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyGur2Mobile,
                      decoration: InputDecoration(
                          labelText: "Guarenter 2 Mobile",
                          hintText: "Guarenter 2 Mobile",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: gur2Mobile,
                      suggestions: suggessions,
                      clearOnSubmit: true,
                      keyboardType: TextInputType.number,
                    );
                  } else {
                    return TextField(
                      controller: gur2Mobile,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          labelText: "Guarenter 2 Mobile",
                          hintText: "Guarenter 2 Mobile",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                    );
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.data()["gur_2_address"] ?? '');
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyGur2Address,
                      decoration: InputDecoration(
                          labelText: "Guarenter 2 Address",
                          hintText: "Guarenter 2 Address",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: gur2Address,
                      suggestions: suggessions,
                      clearOnSubmit: true,
                      textCapitalization: TextCapitalization.words,
                    );
                  } else {
                    return TextField(
                      controller: gur2Address,
                      decoration: InputDecoration(
                          labelText: "Guarenter 2 Address",
                          hintText: "Guarenter 2 Address",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      textCapitalization: TextCapitalization.words,
                    );
                  }
                },
              ),
              SizedBox(
                height: 20,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.data()["gur_3_name"] ?? '');
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyGur3Name,
                      decoration: InputDecoration(
                          labelText: "Guarenter 3 Name",
                          hintText: "Guarenter 3 Name",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: gur3Name,
                      suggestions: suggessions,
                      clearOnSubmit: false,
                      textCapitalization: TextCapitalization.words,
                      textSubmitted: (text) {
                        pr.show();
                        databaseReference.collection("members")
                            .where("gur_3_name", isEqualTo: text)
                            .get().then((value) {
                          pr.hide();
                          setState(() {
                            gur3Mobile.text = value.docs[0].data()["gur_3_mobile"]
                                .toString();
                            gur3Address.text = value.docs[0].data()["gur_3_address"]
                                .toString();
                          });
                        });
                      },
                    );
                  } else {
                    return TextField(
                      controller: gur3Name,
                      decoration: InputDecoration(
                          labelText: "Guarenter 3 Name",
                          hintText: "Guarenter 3 Name",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      textCapitalization: TextCapitalization.words,
                    );
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.data()["gur_3_mobile"] ?? '');
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyGur3Mobile,
                      decoration: InputDecoration(
                          labelText: "Guarenter 3 Mobile",
                          hintText: "Guarenter 3 Mobile",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: gur3Mobile,
                      suggestions: suggessions,
                      clearOnSubmit: true,
                      keyboardType: TextInputType.number,
                    );
                  } else {
                    return TextField(
                      controller: gur3Mobile,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          labelText: "Guarenter 3 Mobile",
                          hintText: "Guarenter 3 Mobile",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                    );
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),
              FutureBuilder<QuerySnapshot>(
                future: databaseReference.collection("members").get(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<String> suggessions = [];

                    snapshot.data.docs.forEach((element) {
                      suggessions.add(element.data()["gur_3_address"] ?? '');
                    });

                    return SimpleAutoCompleteTextField(
                      key: keyGur3Address,
                      decoration: InputDecoration(
                          labelText: "Guarenter 3 Address",
                          hintText: "Guarenter 3 Address",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      controller: gur3Address,
                      suggestions: suggessions,
                      clearOnSubmit: true,
                      textCapitalization: TextCapitalization.words,
                    );
                  } else {
                    return TextField(
                      controller: gur3Address,
                      decoration: InputDecoration(
                          labelText: "Guarenter 3 Address",
                          hintText: "Guarenter 3 Address",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      textCapitalization: TextCapitalization.words,
                    );
                  }
                },
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, right: 20),
                child: ButtonTheme(
                  minWidth: MediaQuery.of(context).size.width,
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {
                      pr.show();
                      createRecord();
                    },
                    child: Text(
                      "SUBMIT",
                      style: TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void createRecord() async {
    DateTime now = DateTime.now();
    String timeStamp = now.millisecondsSinceEpoch.toString();

    await databaseReference.collection("members").doc(timeStamp).set({
      'name': memberName.text,
      'address': address.text,
      'cluster': cluster.text,
      'mobile': mobileNumber.text,
      'nic': nicNumber.text,
      'loan_amount': loanAmount.text,
      'loanTerm': loanTerm.text,
      'interest': interest.text,
      'dd_code': timeStamp.substring(timeStamp.length - 4),
      'gur_1_name': gur1Name.text,
      'gur_1_mobile': gur1Mobile.text,
      'gur_1_address': gur1Address.text,
      'gur_2_name': gur2Name.text,
      'gur_2_mobile': gur2Mobile.text,
      'gur_2_address': gur2Address.text,
      'gur_3_name': gur3Name.text,
      'gur_3_mobile': gur3Mobile.text,
      'gur_3_address': gur3Address.text,
      'toPaid': (int.parse(loanAmount.text) + int.parse(interest.text)).toString(),
      'loan_no': '1',
      'createdAt': now
    }).then((value) {
      addCluster();

      pr.hide();

      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Successfully"),
            content: new Text("New member added successfully"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
//                  Navigator.pushReplacement(context,
//                      MaterialPageRoute(builder: (context) => HomePage()));
                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                      HomePage()), (Route<dynamic> route) => false);
                },
              ),
            ],
          );
        },
      );
    });
  }

  void addCluster() {
    databaseReference
        .collection("clusters")
        .get()
        .then((QuerySnapshot snapshot) {
          bool createNewCluster = true;
      snapshot.docs.forEach((element) {
        if (element.data() != null && element.get("name") == cluster.text)  {
          createNewCluster = false;
        }
      });

      if (createNewCluster) {
        databaseReference
            .collection("clusters")
            .doc()
            .set({
          'name': cluster.text
        });
      }

    });
  }
}
