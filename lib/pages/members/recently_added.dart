import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:loanprocessingapp/pages/members/member_details.dart';
import 'package:progress_dialog/progress_dialog.dart';

import 'edit_member.dart';

class RecentlyAdded extends StatefulWidget {
  @override
  _RecentlyAddedState createState() => _RecentlyAddedState();
}

class _RecentlyAddedState extends State<RecentlyAdded> {
  String _searchKeyword = "";
  String _searchBy = 'name';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Recently Added Members"),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.blue[400],
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Column(
                children: [
                  DropdownButton(
                    focusColor: Colors.white,
                    value: _searchBy,
                    items: [
                      DropdownMenuItem(
                        child: Text("Name"),
                        value: "name",
                      ),
                      DropdownMenuItem(
                        child: Text("NIC"),
                        value: "nic",
                      ),
                      DropdownMenuItem(
                        child: Text("Cluster"),
                        value: "cluster",
                      ),
                      DropdownMenuItem(
                        child: Text("DDA"),
                        value: "dd_code",
                      )
                    ],
                    onChanged: (value) {
                      setState(() {
                        _searchBy = value;
                      });
                    },
                  ),
                  TextField(
                      autofocus: false,
                      decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.search,
                            color: Colors.black45,
                          ),
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Search",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      onChanged: (value) {
                        setState(() {
                          _searchKeyword = value;
                        });
                      },
                    textCapitalization: TextCapitalization.words,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
                stream: (_searchKeyword == "")
                    ? FirebaseFirestore.instance
                        .collection("members")
                        .snapshots()
                    : FirebaseFirestore.instance
                        .collection("members")
                        .where(_searchBy, isGreaterThanOrEqualTo: _searchKeyword)
                        .where(_searchBy, isLessThan: _searchKeyword + 'z')
                        .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData || snapshot.data.docs.length == 0) {
                    return new Column(
                      children: [
                        Icon(
                          Icons.search,
                          size: 80,
                          color: Colors.black26,
                        ),
                        Center(
                            child: Text(
                          "No members found yet!",
                          style: TextStyle(fontSize: 16),
                        )),
                      ],
                    );
                  } else {
                    List<QueryDocumentSnapshot> _list = [];

                    snapshot.data.docs.forEach((element) {
                      if (element.data().containsKey("createdAt")) {
                        var dateToCheck = element.get("createdAt").toDate();
                        final today = DateTime(DateTime.now().year,
                            DateTime.now().month, DateTime.now().day);
                        final aDate = DateTime(
                            dateToCheck.year, dateToCheck.month, dateToCheck.day);

                        if (aDate == today) {
                          _list.add(element);
                        }
                      }
                    });

                    return ListView.builder(
                      padding: EdgeInsets.all(8.0),
                      itemCount: _list.length,
                      itemBuilder: (buildContext, index) =>
                          MemberRow(_list[index]),
                    );
                  }
                }),
          ),
        ],
      ),
    );
  }
}

class MemberRow extends StatefulWidget {
  final DocumentSnapshot member;

  MemberRow(this.member);

  @override
  _MemberRowState createState() => _MemberRowState();
}

class _MemberRowState extends State<MemberRow> {
  final databaseReference = FirebaseFirestore.instance;

  TextEditingController amountController = new TextEditingController();
  TextEditingController deleteController = new TextEditingController();
  TextEditingController interestController = new TextEditingController();
  TextEditingController termsController = new TextEditingController();

  ProgressDialog pr;

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context);

    return Column(
      children: <Widget>[
        Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => MemberDetails(widget.member.id)));
            },
            child: Container(
              color: Colors.white,
              child: new ListTile(
                leading: new CircleAvatar(
                  backgroundColor: Colors.indigoAccent,
                  child: new Icon(Icons.person),
                  foregroundColor: Colors.white,
                ),
                title: Text(widget.member.data()["name"]),
                subtitle:
                    Text("Rs " + widget.member.data()["loan_amount"] + "/="),
              ),
            ),
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
              caption: 'Edit',
              color: Colors.green,
              icon: Icons.edit,
              onTap: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => EditMember(widget.member.id)))
              },
            ),
            IconSlideAction(
              caption: 'Delete',
              color: Colors.red,
              icon: Icons.delete,
              onTap: () => {displayDeleteDialog(context)},
            ),
          ],
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 0.7,
          color: Colors.black12,
        )
      ],
    );
  }

  void displayDeleteDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: new Text("Delete Member"),
        content: Container(
          height: MediaQuery.of(context).size.height * 0.18,
          child: Column(
            children: <Widget>[
              Text(
                  "Are you sure, you need to delete this member from the system?"),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: deleteController,
                decoration: InputDecoration(labelText: "Delete Code"),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter code';
                  }
                  return null;
                },
              ),
            ],
          ),
        ),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("No"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          new FlatButton(
            child: new Text("Yes"),
            onPressed: () {
              if (deleteController.text == "ecl@58694") {
                Navigator.of(context).pop();
                deleteRecord();
              } else {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text("Invalid Code"),
                ));
              }
            },
          ),
        ],
      ),
      barrierDismissible: false,
    );
  }

  void deleteRecord() async {
    await databaseReference
        .collection("members")
        .doc(widget.member.id)
        .delete()
        .then((value) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Successfully"),
            content: new Text("Member deleted successfully"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    });
  }
}
