
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/material.dart' hide Image;
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:image/image.dart';
import 'package:loanprocessingapp/bluetooth/esc_pos_bluetooth.dart';

class PrintDetails extends StatefulWidget {
  AsyncSnapshot<QuerySnapshot> snapshot;
  DocumentSnapshot member;

  PrintDetails(
      this.snapshot,
      this.member
      );

  @override
  _PrintDetailsState createState() => _PrintDetailsState();
}

class _PrintDetailsState extends State<PrintDetails> {

  PrinterBluetoothManager printerManager = PrinterBluetoothManager();
  List<PrinterBluetooth> _devices = [];

  @override
  void initState() {
    super.initState();

    _startScanDevices();

    printerManager.scanResults.listen((devices) async {
      // print('UI: Devices found ${devices.length}');
      setState(() {
        _devices = devices;
      });
    });
  }

  void _startScanDevices() {
    setState(() {
      _devices = [];
    });
    printerManager.startScan(Duration(seconds: 1));
  }

  void _stopScanDevices() {
    printerManager.stopScan();
  }

  Future<Ticket> demoReceipt(PaperSize paper) async {
    final Ticket ticket = Ticket(paper);

    // final ByteData data = await rootBundle.load('assets/logo216.png');
    // final Uint8List bytes = data.buffer.asUint8List();
    // final Image image = decodeImage(bytes);
    //
    // ticket.image(image);

    ticket.text('EASY CREDIT LANKA LTD.',
        styles: PosStyles(align: PosAlign.center, bold: true,
          height: PosTextSize.size2,));

    ticket.text('Morawaka Branch', styles: PosStyles(align: PosAlign.center));
    ticket.text('070 5572342 / 071 9013679', styles: PosStyles(align: PosAlign.center));

    ticket.hr();

    ticket.row([
      PosColumn(text: 'DDA Code', width: 4, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: widget.member.get("dd_code"), width: 8, styles: PosStyles(align: PosAlign.left)),
    ]);

    ticket.row([
      PosColumn(text: 'Cluster', width: 4, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: widget.member.get("cluster"), width: 8, styles: PosStyles(align: PosAlign.left)),
    ]);

    ticket.row([
      PosColumn(text: 'Name', width: 4, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: widget.member.get("name"), width: 8, styles: PosStyles(align: PosAlign.left)),
    ],);

    ticket.row([
      PosColumn(text: 'Installment', width: 3, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: 'Amount', width: 3, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: 'Date', width: 6, styles: PosStyles(align: PosAlign.left)),
    ]);

    if (widget.snapshot.hasData) {
      widget.snapshot.data.docs.asMap().forEach((index, value) {
        ticket.row([
          PosColumn(text: (index+1).toString(), width: 3, styles: PosStyles(align: PosAlign.left)),
          PosColumn(text: value.data()["amount"] + "/=", width: 3, styles: PosStyles(align: PosAlign.left)),
          PosColumn(text: DateFormat('yyyy-MM-dd kk:mm')
            .format(value.data()["date"].toDate()), width: 6, styles: PosStyles(align: PosAlign.left)),
        ]);
      });
    }

    ticket.hr(linesAfter: 1);

    ticket.text('Thank you!',
        styles: PosStyles(align: PosAlign.center, bold: true));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy H:m');
    final String timestamp = formatter.format(now);
    ticket.text(timestamp,
        styles: PosStyles(align: PosAlign.center), linesAfter: 2);

//     Print QR Code from image
//     try {
//       const String qrData = 'example.com';
//       const double qrSize = 200;
//       final uiImg = await QrPainter(
//         data: qrData,
//         version: QrVersions.auto,
//         gapless: false,
//       ).toImageData(qrSize);
//       final dir = await getTemporaryDirectory();
//       final pathName = '${dir.path}/qr_tmp.png';
//       final qrFile = File(pathName);
//       final imgFile = await qrFile.writeAsBytes(uiImg.buffer.asUint8List());
//       final img = decodeImage(imgFile.readAsBytesSync());
//
//       ticket.image(img);
//     } catch (e) {
//       print(e);
//     }

    // Print QR Code using native function
//    ticket.qrcode(widget.memberId);

    ticket.feed(2);
    ticket.cut();
    return ticket;
  }

  void _testPrint(PrinterBluetooth printer) async {
    printerManager.selectPrinter(printer);

    // TODO Don't forget to choose printer's paper
    const PaperSize paper = PaperSize.mm58;

    // TEST PRINT
    // final PosPrintResult res =
    // await printerManager.printTicket(await testTicket(paper));

    // DEMO RECEIPT
    final PosPrintResult res =
    await printerManager.printTicket(await demoReceipt(paper));

    Fluttertoast.showToast(
        msg: res.msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Print Details"),
      ),
      body: ListView.builder(
          itemCount: _devices.length,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () => _testPrint(_devices[index]),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 60,
                    padding: EdgeInsets.only(left: 10),
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.print),
                        SizedBox(width: 10),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(_devices[index].name ?? ''),
                              Text(_devices[index].address),
                              Text(
                                'Click to print a receipt',
                                style: TextStyle(color: Colors.grey[700]),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Divider(),
                ],
              ),
            );
          }),
      floatingActionButton: StreamBuilder<bool>(
        stream: printerManager.isScanningStream,
        initialData: false,
        builder: (c, snapshot) {
          if (snapshot.data) {
            return FloatingActionButton(
              child: Icon(Icons.stop),
              onPressed: _stopScanDevices,
              backgroundColor: Colors.red,
            );
          } else {
            return FloatingActionButton(
              child: Icon(Icons.search),
              onPressed: _startScanDevices,
            );
          }
        },
      ),
    );
  }
}
