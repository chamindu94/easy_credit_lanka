import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class EditMember extends StatefulWidget {
  String memberId;

  EditMember(
      this.memberId
      );

  @override
  _EditMemberState createState() => _EditMemberState();
}

class _EditMemberState extends State<EditMember> {

  TextEditingController memberName = new TextEditingController();
  TextEditingController address = new TextEditingController();
  TextEditingController cluster = new TextEditingController();
  TextEditingController mobileNumber = new TextEditingController();
  TextEditingController nicNumber = new TextEditingController();
  TextEditingController loanAmount = new TextEditingController();
  TextEditingController loanTerm = new TextEditingController();
  TextEditingController interest = new TextEditingController();
  TextEditingController gur1Name = new TextEditingController();
  TextEditingController gur1Mobile = new TextEditingController();
  TextEditingController gur1Address = new TextEditingController();
  TextEditingController gur2Name = new TextEditingController();
  TextEditingController gur2Mobile = new TextEditingController();
  TextEditingController gur2Address = new TextEditingController();
  TextEditingController gur3Name = new TextEditingController();
  TextEditingController gur3Mobile = new TextEditingController();
  TextEditingController gur3Address = new TextEditingController();

  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();

  final databaseReference = FirebaseFirestore.instance;

  ProgressDialog pr;

  @override
  Widget build(BuildContext context) {

    pr = ProgressDialog(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Member"),
      ),

      body: SingleChildScrollView(
        child: StreamBuilder<DocumentSnapshot>(
          stream: FirebaseFirestore.instance.collection("members").doc(widget.memberId).snapshots(),
          builder: (context, snapshot) {

            memberName = TextEditingController(text: snapshot.data.get("name"));
            address = TextEditingController(text: snapshot.data.get("address"));
            cluster = TextEditingController(text: snapshot.data.get("cluster"));
            mobileNumber = TextEditingController(text: snapshot.data.get("mobile"));
            nicNumber = TextEditingController(text: snapshot.data.get("nic"));
            loanAmount = TextEditingController(text: snapshot.data.get("loan_amount"));
            loanTerm = TextEditingController(text: snapshot.data.get("loanTerm"));
            interest = TextEditingController(text: snapshot.data.get("interest"));
            gur1Name = TextEditingController(
                text: snapshot.data.data()["gur_1_name"] ?? '');
            gur1Mobile = TextEditingController(
                text: snapshot.data.data()["gur_1_mobile"] ?? '');
            gur1Address = TextEditingController(
                text: snapshot.data.data()["gur_1_address"] ?? '');
            gur2Name = TextEditingController(
                text: snapshot.data.data()["gur_2_name"] ?? '');
            gur2Mobile = TextEditingController(
                text: snapshot.data.data()["gur_2_mobile"] ?? '');
            gur2Address = TextEditingController(
                text: snapshot.data.data()["gur_2_address"] ?? '');
            gur3Name = TextEditingController(
                text: snapshot.data.data()["gur_3_name"] ?? '');
            gur3Mobile = TextEditingController(
                text: snapshot.data.data()["gur_3_mobile"] ?? '');
            gur3Address = TextEditingController(
                text: snapshot.data.data()["gur_3_address"] ?? '');

            return Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Personal Details",
                      style: TextStyle(color: Colors.blue[200]),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: memberName,
                    decoration: InputDecoration(
                        labelText: "Member Name",
                        hintText: "Member Name",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                    textCapitalization: TextCapitalization.sentences,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: address,
                    decoration: InputDecoration(
                        labelText: "Address",
                        hintText: "Address",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                    textCapitalization: TextCapitalization.sentences,
                  ),
                  SizedBox(
                    height: 10,
                  ),
//                  TextField(
//                    controller: cluster,
//                    decoration: InputDecoration(
//                        labelText: "Cluster",
//                        hintText: "Cluster",
//                        border: OutlineInputBorder(
//                            borderRadius: BorderRadius.circular(5))),
//                    textCapitalization: TextCapitalization.sentences,
//                  ),
                  FutureBuilder<QuerySnapshot>(
                    future: databaseReference.collection("clusters").get(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {

                        List<String> suggessions = [];

                        snapshot.data.docs.forEach((element) {
                          suggessions.add(element.get("name"));
                        });

                        return SimpleAutoCompleteTextField(
                          key: key,
                          decoration: InputDecoration(
                              labelText: "Cluster",
                              hintText: "Cluster",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5))),
                          controller: cluster,
                          suggestions: suggessions,
                          clearOnSubmit: true,
                        );
                      } else {
                        return TextField(
                          controller: cluster,
                          decoration: InputDecoration(
                              labelText: "Cluster",
                              hintText: "Cluster",
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5))),
                          textCapitalization: TextCapitalization.sentences,
                        );
                      }
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: mobileNumber,
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                        labelText: "Mobile Number",
                        hintText: "Mobile Number",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: nicNumber,
                    decoration: InputDecoration(
                        labelText: "NIC Number",
                        hintText: "NIC Number",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Loan Details",
                      style: TextStyle(color: Colors.blue[200]),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: loanAmount,
                    keyboardType: TextInputType.number,
                    enabled: false,
                    decoration: InputDecoration(
                        labelText: "Loan Amount",
                        hintText: "Loan Amount",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: loanTerm,
                    keyboardType: TextInputType.number,
                    enabled: false,
                    decoration: InputDecoration(
                        labelText: "Loan Term",
                        hintText: "Loan Term",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: interest,
                    keyboardType: TextInputType.number,
                    enabled: false,
                    decoration: InputDecoration(
                        labelText: "Interest",
                        hintText: "Interest",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Guarenters Details",
                      style: TextStyle(color: Colors.blue[200]),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: gur1Name,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Guarenter 1 Name",
                        hintText: "Guarenter 1 Name",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: gur1Mobile,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Guarenter 1 Mobile",
                        hintText: "Guarenter 1 Mobile",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: gur1Address,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Guarenter 1 Address",
                        hintText: "Guarenter 1 Address",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: gur2Name,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Guarenter 2 Name",
                        hintText: "Guarenter 2 Name",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: gur2Mobile,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Guarenter 2 Mobile",
                        hintText: "Guarenter 2 Mobile",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: gur2Address,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Guarenter 2 Address",
                        hintText: "Guarenter 2 Address",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: gur3Name,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Guarenter 3 Name",
                        hintText: "Guarenter 3 Name",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: gur3Mobile,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Guarenter 3 Mobile",
                        hintText: "Guarenter 3 Mobile",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: gur3Address,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Guarenter 3 Address",
                        hintText: "Guarenter 3 Address",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20),
                    child: ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width,
                      height: 50.0,
                      child: RaisedButton(
                        onPressed: () {
                          pr.show();
                          createRecord();
                        },
                        child: Text(
                          "SUBMIT",
                          style: TextStyle(color: Colors.white, fontSize: 16.0),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          }
        ),
      ),
    );
  }

  void createRecord() async {
    await databaseReference.collection("members").doc(widget.memberId).update({
      'name': memberName.text,
      'address': address.text,
      'cluster': cluster.text,
      'mobile': mobileNumber.text,
      'nic': nicNumber.text,
      'gur_1_name': gur1Name.text,
      'gur_1_mobile': gur1Mobile.text,
      'gur_1_address': gur1Address.text,
      'gur_2_name': gur2Name.text,
      'gur_2_mobile': gur2Mobile.text,
      'gur_2_address': gur2Address.text,
      'gur_3_name': gur3Name.text,
      'gur_3_mobile': gur3Mobile.text,
      'gur_3_address': gur3Address.text,
    }).then((value) {
      addCluster();

      pr.hide();

      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Successfully"),
            content: new Text("Member updated successfully"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    });
  }

  void addCluster() {
    databaseReference
        .collection("clusters")
        .get()
        .then((QuerySnapshot snapshot) {
      bool createNewCluster = true;
      snapshot.docs.forEach((element) {
        if (element.data() != null && element.get("name") == cluster.text)  {
          createNewCluster = false;
        }
      });

      if (createNewCluster) {
        databaseReference
            .collection("clusters")
            .doc()
            .set({
          'name': cluster.text
        });
      }

    });
  }
}
