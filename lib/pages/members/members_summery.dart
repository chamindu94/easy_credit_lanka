import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loanprocessingapp/pages/members/print_member_list.dart';
import 'package:progress_dialog/progress_dialog.dart';

class MembersSummery extends StatefulWidget {
  @override
  _MembersSummeryState createState() => _MembersSummeryState();
}

class _MembersSummeryState extends State<MembersSummery> {
  AsyncSnapshot<QuerySnapshot> members;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Members Summery"),
        actions: [
          IconButton(
            icon: Icon(Icons.print),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PrintMemberList(members)));
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: FutureBuilder<QuerySnapshot>(
          future: FirebaseFirestore.instance.collection("clusters").get(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: setColumn(snapshot),
              );
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }

  List<Widget> setColumn(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<Widget> columnWidgets = [];
    if (snapshot.hasData) {
      snapshot.data.docs.forEach((element) {
        var widget = Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 4),
                child: Text(
                  element.data()["name"],
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            FutureBuilder<QuerySnapshot>(
                future: FirebaseFirestore
                    .instance
                    .collection("members")
                    .where("cluster", isEqualTo: element.data()["name"])
                    .get(),
                builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Table(
                        columnWidths: {
                          0: FlexColumnWidth(0.7),
                          1: FlexColumnWidth(0.4),
                          2: FlexColumnWidth(0.4)
                        },
                        border: TableBorder.all(
                            color: Colors.black26,
                            width: 1,
                            style: BorderStyle.solid),
                        children: setTableRows(snapshot),
                      );
                    } else {
                      return Center(
                        child: Text(
                          "No Members"
                        ),
                      );
                    }
                },
            )
          ],
        );
        columnWidgets.add(widget);
      });
    } else {
      columnWidgets = [
        Center(
          child: Text("No Clusters"),
        )
      ];
    }
    return columnWidgets;
  }

  List<TableRow> setTableRows(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<TableRow> list = [];
    snapshot.data.docs.forEach((element) {
      var tableRow = TableRow(children: [
        TableCell(
            child: Container(
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 3, right: 3),
          child: Text(
            element.data()["name"],
          ),
        )),
        TableCell(
            child: Container(
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Center(
            child: Text(
              element.data()["dd_code"],
            ),
          ),
        )),
        TableCell(
            child: Container(
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Center(
            child: Text(
              element.data()["toPaid"] + "/=",
            ),
          ),
        )),
      ]);
      list.add(tableRow);
    });

    list.insert(
      0,
      TableRow(children: [
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Name",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "DDA Code",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "To Paid",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ]),
    );

    return list;
  }
}
