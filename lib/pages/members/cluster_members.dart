import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:loanprocessingapp/pages/members/member_details.dart';

class ClusterMembers extends StatefulWidget {
  final String cluster;

  ClusterMembers(this.cluster);

  @override
  _ClusterMembersState createState() => _ClusterMembersState();
}

class _ClusterMembersState extends State<ClusterMembers> {
  String _searchKeyword = "";
  String _searchBy = 'name';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.cluster + " Members"),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.blue[400],
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Column(
                children: [
                  DropdownButton(
                    focusColor: Colors.white,
                    value: _searchBy,
                    items: [
                      DropdownMenuItem(
                        child: Text("Name"),
                        value: "name",
                      ),
                      DropdownMenuItem(
                        child: Text("NIC"),
                        value: "nic",
                      ),
                      DropdownMenuItem(
                        child: Text("DD"),
                        value: "dd",
                      )
                    ],
                    onChanged: (value) {
                      setState(() {
                        _searchBy = value;
                      });
                    },
                  ),
                  TextField(
                      autofocus: false,
                      decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.search,
                            color: Colors.black45,
                          ),
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Search",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      onChanged: (value) {
                        setState(() {
                          _searchKeyword = value;
                        });
                      }),
                ],
              ),
            ),
          ),
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
                stream: (_searchKeyword == "")
                    ? FirebaseFirestore.instance
                        .collection("members")
                        .where("cluster", isEqualTo: widget.cluster)
                        .snapshots()
                    : FirebaseFirestore.instance
                        .collection("members")
                        .where(_searchBy, isEqualTo: _searchKeyword)
                        .where("cluster", isEqualTo: widget.cluster)
                        .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return new Column(
                      children: [
                        Icon(
                          Icons.search,
                          size: 80,
                          color: Colors.black26,
                        ),
                        Center(
                            child: Text(
                          "No members found yet!",
                          style: TextStyle(fontSize: 16),
                        )),
                      ],
                    );
                  } else {
                    return ListView.builder(
                        padding: EdgeInsets.all(8.0),
                        itemCount: snapshot.data.docs.length,
                        // ignore: missing_return
                        itemBuilder: (buildContext, index) {
                          return MemberRow(snapshot.data.docs[index]);
                        });
                  }
                }),
          ),
        ],
      ),
    );
  }
}

class MemberRow extends StatefulWidget {
  final DocumentSnapshot member;

  MemberRow(this.member);

  @override
  _MemberRowState createState() => _MemberRowState();
}

class _MemberRowState extends State<MemberRow> {
  final databaseReference = FirebaseFirestore.instance;

  TextEditingController amountController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => MemberDetails(widget.member.id)));
            },
            child: Container(
              color: Colors.white,
              child: new ListTile(
                leading: new CircleAvatar(
                  backgroundColor: Colors.indigoAccent,
                  child: new Icon(Icons.person),
                  foregroundColor: Colors.white,
                ),
                title: Text(widget.member.data()["name"]),
                subtitle:
                    Text("Rs " + widget.member.data()["loan_amount"] + "/="),
              ),
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 0.7,
          color: Colors.black12,
        )
      ],
    );
  }
}
