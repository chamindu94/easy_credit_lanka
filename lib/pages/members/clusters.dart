import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:loanprocessingapp/pages/members/cluster_members.dart';

class Clusters extends StatefulWidget {
  @override
  _ClustersState createState() => _ClustersState();
}

class _ClustersState extends State<Clusters> {
  String _searchKeyword = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Clusters"),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.blue[400],
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Column(
                children: [
                  TextField(
                    autofocus: false,
                    decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.search,
                          color: Colors.black45,
                        ),
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Search Cluster",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                    onChanged: (value) {
                      setState(() {
                        _searchKeyword = value;
                      });
                    },
                    textCapitalization: TextCapitalization.words,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
                stream: (_searchKeyword == "")
                    ? FirebaseFirestore.instance
                        .collection("clusters")
                        .snapshots()
                    : FirebaseFirestore.instance
                        .collection("clusters")
                        .where("name", isGreaterThanOrEqualTo: _searchKeyword)
                        .where("name", isLessThan: _searchKeyword + 'z')
                        .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData || snapshot.data.docs.length == 0) {
                    return new Column(
                      children: [
                        Icon(
                          Icons.search,
                          size: 80,
                          color: Colors.black26,
                        ),
                        Center(
                            child: Text(
                          "No records found yet!",
                          style: TextStyle(fontSize: 16),
                        )),
                      ],
                    );
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(8.0),
                      itemCount: snapshot.data.docs.length,
                      itemBuilder: (buildContext, index) =>
                          MemberRow(snapshot.data.docs[index]),
                    );
                  }
                }),
          ),
        ],
      ),
    );
  }
}

class MemberRow extends StatefulWidget {
  final DocumentSnapshot member;

  MemberRow(this.member);

  @override
  _MemberRowState createState() => _MemberRowState();
}

class _MemberRowState extends State<MemberRow> {
  final databaseReference = FirebaseFirestore.instance;

  TextEditingController deleteController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          ClusterMembers(widget.member.data()["name"])));
            },
            child: Container(
              color: Colors.white,
              child: new ListTile(
                leading: new CircleAvatar(
                  backgroundColor: Colors.indigoAccent,
                  child: new Icon(Icons.place),
                  foregroundColor: Colors.white,
                ),
                title: Text(widget.member.data()["name"]),
                subtitle: StreamBuilder<QuerySnapshot>(
                    stream: databaseReference.collection("members").snapshots(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        int count = 0;
                        snapshot.data.docs.forEach((element) {
                          if (element.data()["cluster"] ==
                              widget.member.data()["name"]) {
                            count++;
                          }
                        });
                        return Text("Members : " + count.toString());
                      } else {
                        return Text("Members : 0");
                      }
                    }),
              ),
            ),
          ),
          secondaryActions: [
            IconSlideAction(
              caption: 'Delete',
              color: Colors.red,
              icon: Icons.delete,
              onTap: () => {displayDeleteDialog(context)},
            ),
          ],
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 0.7,
          color: Colors.black12,
        )
      ],
    );
  }

  void displayDeleteDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: new Text("Delete Cluster"),
        content: Container(
          height: MediaQuery.of(context).size.height * 0.18,
          child: Column(
            children: <Widget>[
              Text(
                  "Are you sure, you need to delete this cluster from the system?"),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: deleteController,
                decoration: InputDecoration(labelText: "Delete Code"),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter code';
                  }
                  return null;
                },
              ),
            ],
          ),
        ),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("No"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          new FlatButton(
            child: new Text("Yes"),
            onPressed: () {
              if (deleteController.text == "ecl@58694") {
                Navigator.of(context).pop();
                deleteRecord();
              } else {
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text("Invalid Code"),
                ));
              }
            },
          ),
        ],
      ),
      barrierDismissible: false,
    );
  }

  void deleteRecord() async {
    await databaseReference
        .collection("clusters")
        .doc(widget.member.id)
        .delete()
        .then((value) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Successfully"),
            content: new Text("Cluster deleted successfully"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    });
  }
}
