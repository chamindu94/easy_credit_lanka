import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:loanprocessingapp/pages/members/previous_loans.dart';
import 'package:loanprocessingapp/pages/members/print_details.dart';

class MemberDetails extends StatefulWidget {
  String documentId;

  MemberDetails(this.documentId);

  @override
  _MemberDetailsState createState() => _MemberDetailsState();
}

class _MemberDetailsState extends State<MemberDetails> {
  final databaseReference = FirebaseFirestore.instance;
  AsyncSnapshot<QuerySnapshot> docSnapshot;
  AsyncSnapshot<DocumentSnapshot> memberDetails;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Member Details"),
        actions: [
          IconButton(
            icon: Icon(Icons.print),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          PrintDetails(docSnapshot, memberDetails.data)));
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: StreamBuilder<DocumentSnapshot>(
          stream: FirebaseFirestore.instance
              .collection("members")
              .doc(widget.documentId)
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return new Text("No Data");
            } else {
              memberDetails = snapshot;

              int amount = int.parse(snapshot.data.data()["loan_amount"]);
              int interest = int.parse(snapshot.data.data()["interest"]);
              int terms = int.parse(snapshot.data.data()["loanTerm"]);

              String installment =
                  ((amount + interest) / terms).toStringAsFixed(0);

              String loan_no = snapshot.data.data()["loan_no"];

              return Column(
                children: [
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Wrap(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              "Personal Details",
                              style: TextStyle(color: Colors.blue[200]),
                            ),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                width: 130,
                                child: Text(
                                  "Name",
                                  style: TextStyle(color: Colors.black38),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: Text(
                                    snapshot.data.data()["name"],
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                width: 130,
                                child: Text(
                                  "Mobile Number",
                                  style: TextStyle(color: Colors.black38),
                                ),
                              ),
                              Container(
                                child: Text(
                                  snapshot.data.data()["mobile"],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                width: 130,
                                child: Text(
                                  "Address",
                                  style: TextStyle(color: Colors.black38),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: Text(
                                    snapshot.data.data()["address"],
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                width: 130,
                                child: Text(
                                  "Cluster",
                                  style: TextStyle(color: Colors.black38),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: Text(
                                    snapshot.data.data()["cluster"],
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                width: 130,
                                child: Text(
                                  "NIC Number",
                                  style: TextStyle(color: Colors.black38),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: Text(
                                    snapshot.data.data()["nic"],
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                width: 130,
                                child: Text(
                                  "DDA Code",
                                  style: TextStyle(color: Colors.black38),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: Text(
                                    snapshot.data.data()["dd_code"],
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Card(
                    child: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Wrap(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              "Gaurenters Details",
                              style: TextStyle(color: Colors.blue[200]),
                            ),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Container(
                            height: 120,
                            child: PageView(
                              children: [
                                Column(
                                  children: <Widget>[
                                    Row(
                                      children: [
                                        Container(
                                          width: 133,
                                          child: Text(
                                            "Gaurenter 1 Name",
                                            style:
                                                TextStyle(color: Colors.black38),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            snapshot.data.data()["gur_1_name"] ??
                                                '-',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    Row(
                                      children: [
                                        Container(
                                          width: 133,
                                          child: Text(
                                            "Gaurenter 1 Mobile",
                                            style:
                                                TextStyle(color: Colors.black38),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            snapshot.data
                                                    .data()["gur_1_mobile"] ??
                                                '-',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    Row(
                                      children: [
                                        Container(
                                          width: 133,
                                          child: Text(
                                            "Gaurenter 1 Address",
                                            style:
                                                TextStyle(color: Colors.black38),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            snapshot.data
                                                    .data()["gur_1_address"] ??
                                                '-',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Row(
                                      children: [
                                        Container(
                                          width: 133,
                                          child: Text(
                                            "Gaurenter 2 Name",
                                            style: TextStyle(
                                                color: Colors.black38),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            snapshot.data
                                                .data()["gur_2_name"] ?? '-',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    Row(
                                      children: [
                                        Container(
                                          width: 133,
                                          child: Text(
                                            "Gaurenter 2 Mobile",
                                            style: TextStyle(
                                                color: Colors.black38),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            snapshot.data
                                                .data()["gur_2_mobile"] ?? '-',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    Row(
                                      children: [
                                        Container(
                                          width: 133,
                                          child: Text(
                                            "Gaurenter 2 Address",
                                            style: TextStyle(
                                                color: Colors.black38),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            snapshot.data
                                                .data()["gur_2_address"] ?? '-',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Row(
                                      children: [
                                        Container(
                                          width: 133,
                                          child: Text(
                                            "Gaurenter 3 Name",
                                            style: TextStyle(
                                                color: Colors.black38),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            snapshot.data
                                                .data()["gur_3_name"] ?? '-',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    Row(
                                      children: [
                                        Container(
                                          width: 133,
                                          child: Text(
                                            "Gaurenter 3 Mobile",
                                            style: TextStyle(
                                                color: Colors.black38),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            snapshot.data
                                                .data()["gur_3_mobile"] ?? '-',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    Row(
                                      children: [
                                        Container(
                                          width: 133,
                                          child: Text(
                                            "Gaurenter 3 Address",
                                            style: TextStyle(
                                                color: Colors.black38),
                                          ),
                                        ),
                                        Container(
                                          child: Text(
                                            snapshot.data
                                                .data()["gur_3_address"] ?? '-',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.0),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Card(
                    child: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Wrap(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              "Loan Details",
                              style: TextStyle(color: Colors.blue[200]),
                            ),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Loan Amount",
                                      style: TextStyle(color: Colors.black38),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      snapshot.data.data()["loan_amount"] +
                                          "/=",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Interest",
                                      style: TextStyle(color: Colors.black38),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      snapshot.data.data()["interest"] + "/=",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Terms",
                                      style: TextStyle(color: Colors.black38),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      snapshot.data.data()["loanTerm"],
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 0.7,
                              color: Colors.black12,
                            ),
                          ),
                          StreamBuilder<QuerySnapshot>(
                              stream: FirebaseFirestore.instance
                                  .collection("members")
                                  .doc(widget.documentId)
                                  .collection("installments")
                                  .where('loan_no', isEqualTo: loan_no)
                                  .snapshots(),
                              builder: (context, snapshot) {
                                docSnapshot = snapshot;
                                if (snapshot.hasData) {
                                  int total = 0;
                                  snapshot.data.docs.forEach((element) {
                                    total +=
                                        int.parse(element.data()["amount"]);
                                  });

                                  int toBePaid = (amount + interest) - total;

                                  return Column(
                                    children: [
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 150,
                                            child: Text(
                                              "Installment",
                                              style: TextStyle(
                                                  color: Colors.black38),
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              child: Text(
                                                installment + "/=",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16.0),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10.0,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 150,
                                            child: Text(
                                              "Total Paid",
                                              style: TextStyle(
                                                  color: Colors.black38),
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              child: Text(
                                                total.toString() + "/=",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16.0),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10.0,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 150,
                                            child: Text(
                                              "To Be Paid",
                                              style: TextStyle(
                                                  color: Colors.black38),
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              child: Text(
                                                toBePaid.toString() + "/=",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16.0),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  );
                                } else {
                                  return Column(
                                    children: [
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 150,
                                            child: Text(
                                              "Monthly Installment",
                                              style: TextStyle(
                                                  color: Colors.black38),
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              child: Text(
                                                installment + "/=",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16.0),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10.0,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 150,
                                            child: Text(
                                              "Total Paid",
                                              style: TextStyle(
                                                  color: Colors.black38),
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              child: Text(
                                                "0/=",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16.0),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10.0,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 150,
                                            child: Text(
                                              "To Be Paid",
                                              style: TextStyle(
                                                  color: Colors.black38),
                                            ),
                                          ),
                                          Expanded(
                                            child: Container(
                                              child: Text(
                                                "0/=",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16.0),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  );
                                }
                              }),
                          Padding(
                            padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 0.7,
                              color: Colors.black12,
                            ),
                          ),
                          Container(
                            child: StreamBuilder<QuerySnapshot>(
                                stream: FirebaseFirestore.instance
                                    .collection("members")
                                    .doc(widget.documentId)
                                    .collection("installments")
                                    .where("loan_no", isEqualTo: loan_no)
                                    .snapshots(),
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    return Table(
                                      border: TableBorder.all(
                                          color: Colors.black26,
                                          width: 1,
                                          style: BorderStyle.solid),
                                      children: getData(snapshot),
                                    );
                                  } else {
                                    return Text("No Payments Yet");
                                  }
                                }),
                          ),
                          Container(
                              child: FutureBuilder<DocumentSnapshot>(
                                future: databaseReference
                                    .collection("members")
                                    .doc(widget.documentId)
                                    .get(),
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    int loan_nos = int.parse(
                                        snapshot.data.data()["loan_no"].toString());
                                    if (loan_nos > 1) {
                                      return Column(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(
                                                top: 15.0, bottom: 15.0),
                                            child: Container(
                                              width:
                                              MediaQuery.of(context).size.width,
                                              height: 0.7,
                                              color: Colors.black12,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0, right: 20),
                                            child: ButtonTheme(
                                              minWidth:
                                              MediaQuery.of(context).size.width,
                                              height: 50.0,
                                              child: RaisedButton(
                                                onPressed: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              PreviousLoans(
                                                                  widget.documentId,
                                                                  loan_nos)));
                                                },
                                                child: Text(
                                                  "VIEW PREVIOUS LOANS",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16.0),
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      );
                                    } else {
                                      return Container();
                                    }
                                  } else {
                                    return Container();
                                  }
                                },
                              )),
                        ],
                      ),
                    ),
                  )
                ],
              );
            }
          },
        ),
      ),
    );
  }

  getData(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<TableRow> list = [];
    snapshot.data.docs.asMap().forEach((index, element) {
      var tableRow = TableRow(children: [
        TableCell(
            child: Container(
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Center(
            child: Text(
              (index + 1).toString(),
            ),
          ),
        )),
        TableCell(
            child: Container(
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Center(
            child: Text(
              element.data()["amount"] + "/=",
            ),
          ),
        )),
        TableCell(
            child: Container(
          padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Center(
            child: Text(
              DateFormat('yyyy-MM-dd kk:mm')
                  .format(element.data()["date"].toDate()),
            ),
          ),
        )),
      ]);
      list.add(tableRow);
    });

    list.insert(
      0,
      TableRow(children: [
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Installment #",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Amount",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Date & Time",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ]),
    );

    return list;
  }

  TextEditingController amountController = new TextEditingController();

  void displayEditDialog(BuildContext context, String documentId) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: new Text("Mark Installment"),
        content: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          child: Column(
            children: <Widget>[
              TextFormField(
                controller: amountController,
                decoration: InputDecoration(labelText: "Amount"),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter amount';
                  }
                  return null;
                },
                keyboardType: TextInputType.number,
              ),
            ],
          ),
        ),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          new FlatButton(
            child: new Text("Add"),
            onPressed: () {
              Navigator.of(context).pop();
              updateRecord(documentId);
            },
          ),
        ],
      ),
      barrierDismissible: false,
    );
  }

  void updateRecord(String installmentId) async {
    DateTime now = DateTime.now();

    await databaseReference
        .collection("members")
        .doc(widget.documentId)
        .collection("installments")
        .doc(installmentId)
        .update({
      'amount': amountController.text,
      'date': now,
    }).then((value) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Successfully"),
            content: new Text("Installment updated successfully"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    });
  }

  void displayDeleteDialog(BuildContext context, String installmentId) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: new Text("Delete Installment"),
        content: Container(
          height: MediaQuery.of(context).size.height * 0.07,
          child: Column(
            children: <Widget>[
              Text(
                  "Are you sure, you need to delete this record from the system?"),
            ],
          ),
        ),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("No"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          new FlatButton(
            child: new Text("Yes"),
            onPressed: () {
              Navigator.of(context).pop();
              deleteRecord(installmentId);
            },
          ),
        ],
      ),
      barrierDismissible: false,
    );
  }

  void deleteRecord(String installmentId) async {
    await databaseReference
        .collection("members")
        .doc(widget.documentId)
        .collection("installments")
        .doc(installmentId)
        .delete()
        .then((value) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Successfully"),
            content: new Text("Installment deleted successfully"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    });
  }
}
