import 'dart:convert';

Member memberFromJson(String str) {
  final jsonData = json.decode(str);
  return Member.fromMap(jsonData);
}

String memberToJson(Member data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Member {
  String id;
  String address;
  String cluster;
  String dd_code;
  String interest;
  String loanTerm;
  String loan_amount;
  String mobile;
  String name;
  String nic;
  String toPaid;

  Member({
    this.id,
    this.address,
    this.cluster,
    this.dd_code,
    this.interest,
    this.loanTerm,
    this.loan_amount,
    this.mobile,
    this.name,
    this.nic,
    this.toPaid
  });

  factory Member.fromMap(Map<String, dynamic> json) => new Member(
    id: json["id"],
    address: json["address"],
    cluster: json["cluster"],
    dd_code: json["dd_code"],
    interest: json["interest"],
    loanTerm: json["loanTerm"],
    loan_amount: json["loan_amount"],
    mobile: json["mobile"],
    name: json["name"],
    nic: json["nic"],
    toPaid: json["toPaid"],
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "address": address,
    "cluster": cluster,
    "dd_code": dd_code,
    "interest": interest,
    "loanTerm": loanTerm,
    "loan_amount": loan_amount,
    "mobile": mobile,
    "name": name,
    "nic": nic,
    "toPaid": toPaid,
  };
}