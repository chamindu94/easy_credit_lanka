
import 'dart:io';

import 'package:loanprocessingapp/pages/database/models/MemberModel.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null)
      return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "/TestDB.db";
    return await openDatabase(path, version: 1, onOpen: (db) {
    }, onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Members ("
          "id TEXT PRIMARY KEY,"
          "address TEXT,"
          "cluster TEXT,"
          "dd_code TEXT,"
          "interest TEXT,"
          "loanTerm TEXT,"
          "loan_amount TEXT,"
          "mobile TEXT,"
          "name TEXT,"
          "nic TEXT,"
          "toPaid TEXT"
          ")");
    });
  }

  newMember(Member newMember) async {
    final db = await database;
    var res = await db.insert("Members", newMember.toMap());
    return res;
  }

  getMember(String id) async {
    final db = await database;
    var res =await  db.query("Members", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Member.fromMap(res.first) : Null ;
  }

  Future<List<Member>> getAllMembers() async {
    final db = await database;
    var res = await db.query("Members");
    List<Member> list =
    res.isNotEmpty ? res.map((c) => Member.fromMap(c)).toList() : [];
    return list;
  }

  Future deleteAllMembers() async {
    final db = await database;
    db.rawDelete("Delete * from Members");
  }
}