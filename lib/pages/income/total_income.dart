import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';

class TotalIncome extends StatefulWidget {
  @override
  _TotalIncomeState createState() => _TotalIncomeState();
}

class _TotalIncomeState extends State<TotalIncome> {

  TextEditingController codeController = new TextEditingController();
  TextEditingController amountController = new TextEditingController();

  String memberId;
  int previousAmount;
  String incomeId;

  ProgressDialog pr;

  String _cluster = "All";

  DateTime _selectedDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Income Report"),
        backgroundColor: Colors.green,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 10,),
            FlatButton(
                onPressed: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2019, 1, 1),
                      maxTime: DateTime(2500, 12, 31),
                      theme: DatePickerTheme(
                          headerColor: Colors.orange,
                          backgroundColor: Colors.blue,
                          itemStyle: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                          doneStyle:
                          TextStyle(color: Colors.white, fontSize: 16)),
                      onChanged: (date) {
                        print('change $date in time zone ' +
                            date.timeZoneOffset.inHours.toString());
                      }, onConfirm: (date) {
                        setState(() {
                          _selectedDate = date;
                        });
                        print('confirm $date');
                      }, currentTime: _selectedDate, locale: LocaleType.en);
                },
                child: Text(
                  DateFormat('yyyy-MM-dd').format(_selectedDate),
                  style: TextStyle(color: Colors.blue),
                )),
            Divider(
              color: Colors.black38,
              thickness: 0.5,
              height: 25,
            ),
            FutureBuilder<QuerySnapshot>(
              future: FirebaseFirestore.instance.collection("clusters").get(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {

                  List<Widget> _clusterButtons = snapshot.data.docs.map((e) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal:5.0),
                      child: FlatButton(
                        onPressed: () {
                          setState(() {
                            _cluster = e.get("name");
                          });
                        },
                        color: Colors.orange,
                        child: Text(
                          e.get("name"),
                          style: TextStyle(color: Colors.white),
                        ),),
                    );
                  }).toList();

                  _clusterButtons.insert(0,
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal:5.0),
                      child: FlatButton(
                          onPressed: () {
                            setState(() {
                              _cluster = "All";
                            });
                          },
                          color: Colors.orange,
                          child: Text(
                              "All Clusters",
                            style: TextStyle(color: Colors.white),
                          )
                      ),
                    ),
                  );

                  return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: _clusterButtons,
                    ),
                  );
                } else {
                  return Container();
                }
              },
            ),
            Divider(color: Colors.black38, thickness: 0.5, height: 25,),
            Text("Cluster - $_cluster"),
            SizedBox(height: 30,),
            FutureBuilder<QuerySnapshot>(
              future: FirebaseFirestore.instance.collection("members").get(),
              builder: (context, snapshot) {
                int toPaid = 0;
                if (snapshot.hasData) {
                  snapshot.data.docs.forEach((member) {
                    if (_cluster != "All") {
                      if (_cluster == member.data()["cluster"]) {
                        toPaid += int.parse(member.data()["toPaid"]);
                      }
                    } else {
                      toPaid += int.parse(member.data()["toPaid"]);
                    }
                  });

                  return Container(
                      alignment: Alignment.center,
                      child: Column(children: [
                        Text(
                          "To Received",
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          "Rs." + toPaid.toString() + "/=",
                          style: TextStyle(
                              fontSize: 30,
                              color: Colors.green,
                              fontWeight: FontWeight.bold),
                        )
                      ]));
                } else {
                  return Container(
                      alignment: Alignment.center,
                      child: Column(children: [
                        Text(
                          "To Received",
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          "Rs.0/=",
                          style: TextStyle(
                              fontSize: 30,
                              color: Colors.green,
                              fontWeight: FontWeight.bold),
                        )
                      ]));
                }
              },
            ),
            SizedBox(height: 30,),
            FutureBuilder(
              future: FirebaseFirestore.instance.collection("income").get(),
              builder: (context, snapshot) {
                int todayIncome = 0;
                if (snapshot.hasData) {
                  snapshot.data.docs.forEach((element) {
                    var dateToCheck = element.data()["date"].toDate();
                    final today = DateTime(_selectedDate.year,
                        _selectedDate.month, _selectedDate.day);
                    final aDate = DateTime(
                        dateToCheck.year, dateToCheck.month, dateToCheck.day);

                    if (aDate == today) {
                      if (_cluster != "All") {
                        if (_cluster == element.data()["cluster"]) {
                          todayIncome += int.parse(element.data()["amount"]);
                        }
                      } else {
                        todayIncome += int.parse(element.data()["amount"]);
                      }
                    }
                  });
                  return Container(
                      alignment: Alignment.center,
                      child: Column(children: [
                        Text(
                          "Today Income",
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          "Rs." + todayIncome.toString() + "/=",
                          style: TextStyle(
                              fontSize: 30,
                              color: Colors.green,
                              fontWeight: FontWeight.bold),
                        )
                      ]));
                } else {
                  return Container(
                      alignment: Alignment.center,
                      child: Column(children: [
                        Text(
                          "Today Income",
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          "Rs.0/=",
                          style: TextStyle(
                              fontSize: 30,
                              color: Colors.green,
                              fontWeight: FontWeight.bold),
                        )
                      ]));
                }
              },
            ),
            SizedBox(height: 30,),
            FutureBuilder<QuerySnapshot>(
              future: FirebaseFirestore.instance.collection("income").get(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Table(
                    columnWidths: {
                      0: FlexColumnWidth(0.6),
                      1: FlexColumnWidth(0.7),
                      3: FlexColumnWidth(0.4)
                    },
                    border: TableBorder.all(
                        color: Colors.black26,
                        width: 1,
                        style: BorderStyle.solid
                    ),
                    children: setTableRows(snapshot),
                  );
                } else {
                  return Container();
                }
              },
            )
          ],
        ),
      ),
    );
  }

  List<TableRow> setTableRows(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<TableRow> list = [];
    snapshot.data.docs.forEach((element) {
      var dateToCheck = element.data()["date"].toDate();
      final today =
      DateTime(_selectedDate.year, _selectedDate.month, _selectedDate.day);
      final aDate =
      DateTime(dateToCheck.year, dateToCheck.month, dateToCheck.day);

      if (aDate == today) {
        if (_cluster != "All") {
          if (_cluster == element.data()["cluster"]) {
            var tableRow = TableRow(children: [
              TableCell(
                  child: Container(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: Center(
                      child: Text(
                        element.data()["amount"] + "/=",
                      ),
                    ),
                  )),
              TableCell(
                  child: Container(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: Center(
                      child: Text(
                        element.data()["added_by"],
                      ),
                    ),
                  )),
              TableCell(
                  child: Container(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: Center(
                      child: StreamBuilder<DocumentSnapshot>(
                          stream: FirebaseFirestore.instance
                              .collection("members")
                              .doc(element.data()["loan_member"])
                              .snapshots(),
                          builder: (context, snapshot) {
                            if (snapshot.hasData && snapshot.data.data() != null) {
                              return Text(
                                snapshot.data.data()["name"],
                              );
                            } else {
                              return Text("Member deleted");
                            }
                          }),
                    ),
                  )),
              TableCell(
                child: Container(
                  padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Center(
                    child: IconButton(
                      icon: Icon(Icons.edit, color: Colors.green,),
                      onPressed: () {
                        memberId = element.data()["loan_member"];
                        previousAmount = int.parse(element.data()["amount"]);
                        incomeId = element.id;
                        displaySecretCode();
                      },
                    ),
                  ),
                ),
              )
            ]);
            list.add(tableRow);
          }
        } else {
          var tableRow = TableRow(children: [
            TableCell(
                child: Container(
                  padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Center(
                    child: Text(
                      element.data()["amount"] + "/=",
                    ),
                  ),
                )),
            TableCell(
                child: Container(
                  padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Center(
                    child: Text(
                      element.data()["added_by"],
                    ),
                  ),
                )),
            TableCell(
                child: Container(
                  padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 5),
                  child: StreamBuilder<DocumentSnapshot>(
                      stream: FirebaseFirestore.instance
                          .collection("members")
                          .doc(element.data()["loan_member"])
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData && snapshot.data.data() != null) {
                          return Text(
                            snapshot.data.data()["name"],
                          );
                        } else {
                          return Text("Member deleted");
                        }
                      }),
                )),
            TableCell(
              child: Container(
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Center(
                  child: IconButton(
                    icon: Icon(Icons.delete, color: Colors.red,),
                    onPressed: () {
                      memberId = element.data()["loan_member"];
                      previousAmount = int.parse(element.data()["amount"]);
                      incomeId = element.id;
                      displaySecretCode();
                    },
                  ),
                ),
              ),
            )
          ]);
          list.add(tableRow);
        }
      }
    });

    list.insert(
      0,
      TableRow(children: [
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Amount",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Added By",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Loan Member",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Action",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ]),
    );

    return list;
  }

  void displaySecretCode() {
    showDialog(
      context: context,
      builder: (_) =>
          AlertDialog(
            title: new Text("Enter code"),
            content: Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.2,
              child: Column(
                children: <Widget>[
                  Text("Please enter code to delete this installment"),
                  SizedBox(height: 10,),
                  TextFormField(
                    controller: codeController,
                    decoration: InputDecoration(labelText: "Code"),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter code';
                      }
                      return null;
                    },
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("No"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new FlatButton(
                child: new Text("Yes"),
                onPressed: () {
                  if (codeController.text == "ecl@58694") {
                    Navigator.of(context).pop();
                    pr.show();
                    deleteRecord();
                  } else {
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text("Invalid Code"),
                    ));
                  }
                },
              ),
            ],
          ),
      barrierDismissible: false,

    );
  }

  int newToPaid;

  void deleteRecord() {
    //get current toPaid amount
    FirebaseFirestore
        .instance
        .collection("members")
        .doc(memberId)
        .get().then((member) {

          //add previous amount
      newToPaid = int.parse(member.get("toPaid")) + previousAmount;

      //delete income document
      FirebaseFirestore
        .instance
        .collection("income")
        .doc(incomeId)
        .delete().then((value) {

        //get last installment of the member
        FirebaseFirestore
            .instance
            .collection("members")
            .doc(memberId)
            .collection("installments")
            .orderBy("date", descending: true)
            .limit(1)
            .get().then((installment) {

              String installmentId = installment.docs[0].id;

              //delete installment
              FirebaseFirestore
                  .instance
                  .collection("members")
                  .doc(memberId)
                  .collection("installments")
                  .doc(installmentId)
                  .delete().then((value) {

                //update to paid amount
                FirebaseFirestore
                  .instance
                    .collection("members")
                    .doc(memberId)
                    .update({
                  'toPaid': newToPaid.toString()
                }).then((value) {

                  codeController = TextEditingController(text: "");
                  amountController = TextEditingController(text: "");

                  //create log
                  FirebaseFirestore.instance
                      .collection("members")
                      .doc(memberId)
                      .get()
                      .then((value) {
                        var memberName;
                        if (value.exists) {
                          memberName = value.get("name");
                        } else {
                          memberName = "Member deleted";
                        }

                        DateTime now = DateTime.now();

                        FlutterSession().get("username").then((username) {
                          FirebaseFirestore.instance
                              .collection("logs")
                              .doc(now.millisecondsSinceEpoch.toString())
                              .set({
                            'log': "$username deleted $memberName's installment of Rs. $previousAmount/= at $now",
                            'date': now
                          }).then((value) {
                            pr.hide();

                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                // return object of type Dialog
                                return AlertDialog(
                                  title: new Text("Successfully"),
                                  content: new Text("Installment deleted successfully"),
                                  actions: <Widget>[
                                    // usually buttons at the bottom of the dialog
                                    new FlatButton(
                                      child: new Text("Close"),
                                      onPressed: () {
                                        Navigator.pop(context);
                                        setState(() {

                                        });
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          });
                        });
                  });
                });
              });
        });

      });
    });
  }

}
