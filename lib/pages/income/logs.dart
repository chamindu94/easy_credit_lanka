import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Logs extends StatefulWidget {
  @override
  _LogsState createState() => _LogsState();
}

class _LogsState extends State<Logs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Logs"),
        backgroundColor: Colors.green,
      ),
      body: SingleChildScrollView(
        child: Container(
          child: StreamBuilder(
              stream: FirebaseFirestore.instance.collection("logs").snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Table(
                    border: TableBorder.all(
                        color: Colors.black26,
                        width: 1,
                        style: BorderStyle.solid
                    ),
                    columnWidths: {
                      0: FlexColumnWidth(0.15)
                    },
                    children: getRableRows(snapshot),
                  );
                } else {
                  return Container();
                }
              },),
        ),
      ),
    );
  }

  List<TableRow> getRableRows(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<TableRow> list = [];
    snapshot.data.docs.asMap().forEach((index, element) {
      var dateToCheck = element.data()["date"].toDate();
      final today = DateTime(
          DateTime
              .now()
              .year, DateTime
          .now()
          .month, DateTime
          .now()
          .day);
      final aDate =
      DateTime(dateToCheck.year, dateToCheck.month, dateToCheck.day);

      if (aDate == today) {

        var tableRow = TableRow(children: [
          TableCell(
              child: Container(
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Center(
                  child: Text(
                    (index + 1).toString(),
                  ),
                ),
              )),
          TableCell(
              child: Container(
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Center(
                  child: Text(
                    element.data()["log"],
                  ),
                ),
              )),
        ]);
        list.add(tableRow);
      }
    });

    list.insert(
      0,
      TableRow(children: [
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "#",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Log Description",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ]),
    );

    return list;
  }
}
