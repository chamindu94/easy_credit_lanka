import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';

class Collections extends StatefulWidget {
  @override
  _CollectionsState createState() => _CollectionsState();
}

class _CollectionsState extends State<Collections> {
  ProgressDialog pr;

  DateTime _selectedDate = DateTime.now();

  String _user = "All";

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Collections"),
        backgroundColor: Colors.green,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            FlatButton(
                onPressed: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2019, 1, 1),
                      maxTime: DateTime(2500, 12, 31),
                      theme: DatePickerTheme(
                          headerColor: Colors.orange,
                          backgroundColor: Colors.blue,
                          itemStyle: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                          doneStyle:
                              TextStyle(color: Colors.white, fontSize: 16)),
                      onChanged: (date) {
                    print('change $date in time zone ' +
                        date.timeZoneOffset.inHours.toString());
                  }, onConfirm: (date) {
                    setState(() {
                      _selectedDate = date;
                    });
                    print('confirm $date');
                  }, currentTime: _selectedDate, locale: LocaleType.en);
                },
                child: Text(
                  DateFormat('yyyy-MM-dd').format(_selectedDate),
                  style: TextStyle(color: Colors.blue),
                )),
            Divider(
              color: Colors.black38,
              thickness: 0.5,
              height: 25,
            ),
            FutureBuilder<QuerySnapshot>(
              future: FirebaseFirestore.instance.collection("users").get(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<Widget> _clusterButtons = snapshot.data.docs.map((e) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: FlatButton(
                        onPressed: () {
                          setState(() {
                            _user = e.get("username");
                          });
                        },
                        color: Colors.deepPurpleAccent,
                        child: Text(
                          e.get("username"),
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    );
                  }).toList();

                  _clusterButtons.insert(
                    0,
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5.0),
                      child: FlatButton(
                          onPressed: () {
                            setState(() {
                              _user = "All";
                            });
                          },
                          color: Colors.deepPurpleAccent,
                          child: Text(
                            "All Users",
                            style: TextStyle(color: Colors.white),
                          )),
                    ),
                  );

                  return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: _clusterButtons,
                    ),
                  );
                } else {
                  return Container();
                }
              },
            ),
            Divider(
              color: Colors.black38,
              thickness: 0.5,
              height: 25,
            ),
            Align(
              alignment: Alignment.center,
              child: Text("User - $_user"),
            ),
            SizedBox(
              height: 30,
            ),
            Align(
              alignment: Alignment.center,
              child: FutureBuilder<QuerySnapshot>(
                future: FirebaseFirestore.instance.collection("income").get(),
                builder: (context, snapshot) {
                  int collection = 0;
                  if (snapshot.hasData) {
                    snapshot.data.docs.forEach((income) {
                      var dateToCheck = income.data()["date"].toDate();
                      final today = DateTime(_selectedDate.year,
                          _selectedDate.month, _selectedDate.day);
                      final aDate = DateTime(
                          dateToCheck.year, dateToCheck.month, dateToCheck.day);

                      if (aDate == today) {
                        if (_user != "All") {
                          if (_user == income.data()["added_by"]) {
                            collection += int.parse(income.data()["amount"]);
                          }
                        } else {
                          collection += int.parse(income.data()["amount"]);
                        }
                      }
                    });

                    return Container(
                        alignment: Alignment.center,
                        child: Column(children: [
                          Text(
                            "Collection",
                            style: TextStyle(fontSize: 20),
                          ),
                          Text(
                            "Rs." + collection.toString() + "/=",
                            style: TextStyle(
                                fontSize: 30,
                                color: Colors.green,
                                fontWeight: FontWeight.bold),
                          )
                        ]));
                  } else {
                    return Container(
                        alignment: Alignment.center,
                        child: Column(children: [
                          Text(
                            "Collection",
                            style: TextStyle(fontSize: 20),
                          ),
                          Text(
                            "Rs.0/=",
                            style: TextStyle(
                                fontSize: 30,
                                color: Colors.green,
                                fontWeight: FontWeight.bold),
                          )
                        ]));
                  }
                },
              ),
            ),
            SizedBox(height: 30),
            FutureBuilder<QuerySnapshot>(
              future: FirebaseFirestore.instance.collection("income").get(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Table(
                    columnWidths: {
                      0: FlexColumnWidth(0.6),
                      1: FlexColumnWidth(0.7),
                      3: FlexColumnWidth(0.4)
                    },
                    border: TableBorder.all(
                        color: Colors.black26,
                        width: 1,
                        style: BorderStyle.solid),
                    children: setTableRows(snapshot),
                  );
                } else {
                  return Container();
                }
              },
            )
          ],
        ),
      ),
    );
  }

  List<TableRow> setTableRows(AsyncSnapshot<QuerySnapshot> snapshot) {
    List<TableRow> list = [];
    snapshot.data.docs.forEach((element) {
      var dateToCheck = element.data()["date"].toDate();
      final today =
          DateTime(_selectedDate.year, _selectedDate.month, _selectedDate.day);
      final aDate =
          DateTime(dateToCheck.year, dateToCheck.month, dateToCheck.day);

      if (aDate == today) {
        if (_user != "All") {
          if (_user == element.data()["added_by"]) {
            var tableRow = TableRow(children: [
              TableCell(
                  child: Container(
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Center(
                  child: Text(
                    element.data()["amount"] + "/=",
                  ),
                ),
              )),
              TableCell(
                  child: Container(
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Center(
                  child: Text(
                    element.data()["added_by"],
                  ),
                ),
              )),
              TableCell(
                  child: Container(
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 5),
                child: StreamBuilder<DocumentSnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection("members")
                        .doc(element.data()["loan_member"])
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data.data() != null) {
                        return Text(
                          snapshot.data.data()["name"],
                        );
                      } else {
                        return Text("Member deleted");
                      }
                    }),
              )),
            ]);
            list.add(tableRow);
          }
        } else {
          var tableRow = TableRow(children: [
            TableCell(
                child: Container(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Center(
                child: Text(
                  element.data()["amount"] + "/=",
                ),
              ),
            )),
            TableCell(
                child: Container(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Center(
                child: Text(
                  element.data()["added_by"],
                ),
              ),
            )),
            TableCell(
                child: Container(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: Center(
                child: StreamBuilder<DocumentSnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection("members")
                        .doc(element.data()["loan_member"])
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data.data() != null) {
                        return Text(
                          snapshot.data.data()["name"],
                        );
                      } else {
                        return Text("Member deleted");
                      }
                    }),
              ),
            )),
          ]);
          list.add(tableRow);
        }
      }
    });

    list.insert(
      0,
      TableRow(children: [
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Amount",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Added By",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        TableCell(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Center(
              child: Text(
                "Loan Member",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ]),
    );

    return list;
  }
}
