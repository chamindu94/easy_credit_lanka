import 'package:flutter/material.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:loanprocessingapp/pages/login/login_page.dart';

class Logout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: IconButton(
          icon: Icon(Icons.exit_to_app),
          onPressed: () {
            saveSessionData().then((value) {
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage()));
            });
          },
        ),
      ),
    );
  }

  Future<void> saveSessionData() async {
    var session = FlutterSession();
    await session.set("isLogin", "false");
    await session.set("username", "");
    await session.set("userId", "");
  }
}
