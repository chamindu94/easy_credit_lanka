import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:loanprocessingapp/pages/home_page.dart';
import 'package:progress_dialog/progress_dialog.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final databaseReference = FirebaseFirestore.instance;

  String _errorText = "";

  ProgressDialog pr;

  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {

    pr = ProgressDialog(context, type: ProgressDialogType.Normal, isDismissible: false);

    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 60,),
              Center(child: Image.asset("assets/logo.png", width: MediaQuery.of(context).size.width/1.7,)),
              SizedBox(height: 50,),
              Text(_errorText, style: TextStyle(color: Colors.red, fontSize: 18),),
              SizedBox(height: 5,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: Column(
                  children: [
                    TextField(
                      controller: usernameController,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.account_circle),
                          labelText: "Username",
                          hintText: "Username",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                    ),
                    SizedBox(height: 10,),
                    TextField(
                      controller: passwordController,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.lock),
                          labelText: "Password",
                          hintText: "Password",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      obscureText: true,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 50),
                child: ButtonTheme(
                  minWidth: MediaQuery.of(context).size.width,
                  height: 50.0,
                  child: RaisedButton(
                    onPressed: () {
                      pr.show();
                      checkLogin();
                    },
                    child: Text(
                      "LOGIN",
                      style: TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void checkLogin() {
    databaseReference
        .collection("users")
        .get()
        .then((QuerySnapshot snapshot) {
          snapshot.docs.forEach((element) {
            String username = element.get("username");
            String password = element.get("password");
            String role = element.get("role");
            String id = element.id;

            if (usernameController.text == username && passwordController.text == password) {
              saveSessionData(id, role).then((value) {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomePage()));
              });
            } else {
              pr.hide();
              setState(() {
                _errorText = "Invalid Login Details";
              });
            }
          });
    });
  }

  Future<void> saveSessionData(String id, String role) async {
    var session = FlutterSession();
    await session.set("isLogin", "true");
    await session.set("username", usernameController.text);
    await session.set("userId", id);
    await session.set("role", role);
  }

}
