import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:loanprocessingapp/pages/payment/print_receipt.dart';
import 'package:progress_dialog/progress_dialog.dart';

class PendingPayments extends StatefulWidget {
  @override
  _PendingPaymentsState createState() => _PendingPaymentsState();
}

class _PendingPaymentsState extends State<PendingPayments> {

  String _searchKeyword = "";
  String _searchBy = 'name';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Text("Add Payment"),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.orange[400],
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Column(
                children: [
                  DropdownButton(
                    focusColor: Colors.white,
                    value: _searchBy,
                    items: [
                      DropdownMenuItem(
                        child: Text("Name"),
                        value: "name",
                      ),
                      DropdownMenuItem(
                        child: Text("NIC"),
                        value: "nic",
                      ),
                      DropdownMenuItem(
                        child: Text("Center"),
                        value: "cluster",
                      ),
                      DropdownMenuItem(
                        child: Text("DDA"),
                        value: "dd_code",
                      )
                    ],
                    onChanged: (value) {
                      setState(() {
                        _searchBy = value;
                      });
                    },
                  ),
                  TextField(
                    autofocus: false,
                    decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.search,
                          color: Colors.black45,
                        ),
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Search",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                    onChanged: (value) {
                      setState(() {
                        _searchKeyword = value;
                      });
                    },
                    textCapitalization: TextCapitalization.words,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
                stream: (_searchKeyword == "")
                    ? FirebaseFirestore.instance
                    .collection("members")
                    .snapshots()
                    : FirebaseFirestore.instance
                    .collection("members")
                    .where(_searchBy, isGreaterThanOrEqualTo: _searchKeyword)
                    .where(_searchBy, isLessThan: _searchKeyword + 'z')
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return new Column(
                      children: [
                        Icon(Icons.search, size: 80, color: Colors.black26,),
                        Center(child: Text("No members found yet!", style: TextStyle(fontSize: 16),)),
                      ],
                    );
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(8.0),
                      itemCount: snapshot.data.docs.length,
                      itemBuilder: (buildContext, index) =>
                          MemberRow(snapshot.data.docs[index]),
                    );
                  }
                }),
          ),
        ],
      ),
    );
  }
}

class MemberRow extends StatefulWidget {
  final DocumentSnapshot member;

  MemberRow(this.member);

  @override
  _MemberRowState createState() => _MemberRowState();
}

class _MemberRowState extends State<MemberRow> {
  final databaseReference = FirebaseFirestore.instance;

  TextEditingController amountController = new TextEditingController();

  double installment = 0;
  int loanAmount = 0;

  ProgressDialog pr;

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context);

    return StreamBuilder<QuerySnapshot>(
        stream: databaseReference
            .collection("members")
            .doc(widget.member.id)
            .collection("installments")
            .orderBy("date", descending: true)
            .limit(1)
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data.docs.length > 0) {
            final dateToCheck =
            snapshot.data.docs[0].data()["date"].toDate();
            final today = DateTime(DateTime.now().year,
                DateTime.now().month, DateTime.now().day);
            final aDate = DateTime(dateToCheck.year,
                dateToCheck.month, dateToCheck.day);

            if (aDate == today) {
              return Container();
            } else {
              return Column(
                children: <Widget>[
                  Slidable(
                    actionPane: SlidableDrawerActionPane(),
                    actionExtentRatio: 0.25,
                    child: GestureDetector(
                      onTap: () {
                        int amount = int.parse(widget.member.data()["loan_amount"]);
                        int interest = int.parse(widget.member.data()["interest"]);
                        int terms = int.parse(widget.member.data()["loanTerm"]);

                        installment = (amount + interest) / terms;
                        loanAmount = amount + interest;

                        amountController = new TextEditingController(
                            text: installment.toStringAsFixed(0));

                        displayDialog(context);
                      },
                      child: Container(
                        color: Colors.white,
                        child: new ListTile(
                          leading: new CircleAvatar(
                            backgroundColor: Colors.orange,
                            child: new Icon(Icons.person),
                            foregroundColor: Colors.white,
                          ),
                          title: Text(widget.member.data()["name"]),
                          subtitle: Text(
                            "Pending",
                            style: TextStyle(
                                color: Colors.red, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 0.7,
                    color: Colors.black12,
                  )
                ],
              );
            }
          } else {
            return Container();
          }
        }
    );
  }

  void displayDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: new Text("Mark Installment"),
        content: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          child: Column(
            children: <Widget>[
              TextFormField(
                controller: amountController,
                decoration: InputDecoration(labelText: "Amount"),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter amount';
                  }
                  return null;
                },
                keyboardType: TextInputType.number,
              ),
            ],
          ),
        ),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          new FlatButton(
            child: new Text("Add"),
            onPressed: () {
              Navigator.of(context).pop();
              pr.show();

              int total = 0;
              databaseReference
                  .collection("members")
                  .doc(widget.member.id)
                  .collection("installments")
                  .get()
                  .then((QuerySnapshot snapshot) {
                snapshot.docs.forEach((element) {
                  total += int.parse(element.data()["amount"]);
                });
              }).then((value) {
                int amountToPay =
                    loanAmount - total - int.parse(amountController.text);

                pr.hide();

                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PrintReceipt(
                            loanAmount.toString(),
                            installment.toStringAsFixed(0),
                            amountController.text,
                            amountToPay.toString(),
                            widget.member)));
//                    Navigator.push(context,
//                        MaterialPageRoute(builder: (context) => PrintTwo(),));
              });
            },
          ),
        ],
      ),
      barrierDismissible: false,
    );
  }
}
