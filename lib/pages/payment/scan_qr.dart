import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loanprocessingapp/pages/payment/print_receipt.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:qrscan/qrscan.dart' as scanner;

class ScanQr extends StatefulWidget {
  String barcode;

  ScanQr(this.barcode);

  @override
  _ScanQrState createState() => _ScanQrState();
}

class _ScanQrState extends State<ScanQr> {

  final databaseReference = FirebaseFirestore.instance;
  var amountController = new TextEditingController();

  DocumentSnapshot member_snapshot;

  int loanAmount;
  String installment;

  ProgressDialog pr;

  @override
  Widget build(BuildContext context) {

    pr = ProgressDialog(context);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange,
          title: Text("Member Details"),
        ),
        body: SingleChildScrollView(
          child: StreamBuilder<DocumentSnapshot>(
            stream: FirebaseFirestore.instance
                .collection("members")
                .doc(widget.barcode)
                .snapshots(),
            builder: (context, snapshot) {
              member_snapshot = snapshot.data;
              int amount = int.parse(snapshot.data.data()["loan_amount"]);
              int interest = int.parse(snapshot.data.data()["interest"]);
              int terms = int.parse(snapshot.data.data()["loanTerm"]);

              installment =
              ((amount + interest) / terms).toStringAsFixed(0);

              loanAmount = amount + interest;

              amountController = new TextEditingController(text: installment);

              if (!snapshot.hasData) {
                return new Text("No Data");
              } else {
                return Column(
                  children: [
                    Card(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Wrap(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.center,
                              child: Text(
                                "Personal Details",
                                style: TextStyle(color: Colors.blue[200]),
                              ),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 130,
                                  child: Text(
                                    "Name",
                                    style: TextStyle(color: Colors.black38),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      snapshot.data.data()["name"],
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 130,
                                  child: Text(
                                    "Mobile Number",
                                    style: TextStyle(color: Colors.black38),
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    snapshot.data.data()["mobile"],
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 130,
                                  child: Text(
                                    "Address",
                                    style: TextStyle(color: Colors.black38),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      snapshot.data.data()["address"],
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 130,
                                  child: Text(
                                    "Cluster",
                                    style: TextStyle(color: Colors.black38),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      snapshot.data.data()["cluster"],
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 130,
                                  child: Text(
                                    "NIC Number",
                                    style: TextStyle(color: Colors.black38),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      snapshot.data.data()["nic"],
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 20,),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20),
                      child: ButtonTheme(
                        buttonColor: Colors.orange,
                        minWidth: MediaQuery.of(context).size.width,
                        height: 50.0,
                        child: RaisedButton(
                          onPressed: () {
                            displayDialog(context);
                          },
                          child: Text(
                            "Mark Installment",
                            style: TextStyle(color: Colors.white, fontSize: 16.0),
                          ),
                        ),
                      ),
                    )
                  ],
                );
              }
            },
          ),
        ));
  }

  void displayDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) =>
          AlertDialog(
            title: new Text("Mark Installment"),
            content: Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.1,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    controller: amountController,
                    decoration: InputDecoration(labelText: "Amount"),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter amount';
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new FlatButton(
                child: new Text("Add"),
                onPressed: () {
                  databaseReference
                      .collection("members")
                      .doc(widget.barcode)
                      .get()
                      .then((memberDetails) {
                    int to_paid = int.parse(memberDetails.data()["toPaid"]);
                    int amount = int.parse(amountController.text);

                    if (amount > to_paid) {
                      Fluttertoast.showToast(
                          msg: "Balance exceeded. Only $to_paid to pay",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0);
                    } else {}
                  });

                  Navigator.of(context).pop();
                  pr.show();

                  int total = 0;
                  databaseReference
                      .collection("members")
                      .doc(widget.barcode)
                      .collection("installments")
                      .where('loan_no', isEqualTo: member_snapshot.data()["loan_no"])
                      .get()
                      .then((QuerySnapshot snapshot) {
                    snapshot.docs.forEach((element) {
                      total += int.parse(element.data()["amount"]);
                    });
                  }).then((value) {
                    int amountToPay =
                        loanAmount - total - int.parse(amountController.text);

                    pr.hide();

                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PrintReceipt(
                                loanAmount.toString(),
                                installment,
                                amountController.text,
                                amountToPay.toString(),
                                member_snapshot)));
//                    Navigator.push(context,
//                        MaterialPageRoute(builder: (context) => PrintTwo(),));
                  });
                },
              ),
            ],
          ),
      barrierDismissible: false,

    );
  }

  void createRecord() async {
    DateTime now = DateTime.now();

    await databaseReference
        .collection("members")
        .doc(widget.barcode)
        .collection("installments")
        .doc(now.millisecondsSinceEpoch.toString())
        .set({
      'amount': amountController.text,
      'date': now,
    }).then((value) {
      //create record for today income
      FlutterSession().get("username").then((username) {
        databaseReference
            .collection("income")
            .doc()
            .set({
          'amount': amountController.text,
          'added_by': username,
          'loan_member': widget.barcode,
          'date': now
        }).then((value) {


        });
      });
    });
  }
}
