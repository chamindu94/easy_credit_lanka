import 'package:flutter/material.dart';
import 'package:loanprocessingapp/pages/database/Database.dart';
import 'package:loanprocessingapp/pages/database/models/MemberModel.dart';

class OfflinePayment extends StatefulWidget {
  @override
  _OfflinePaymentState createState() => _OfflinePaymentState();
}

class _OfflinePaymentState extends State<OfflinePayment> {

  String _searchKeyword = "";
  String _searchBy = 'name';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Text("Offline Payment"),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.orange[400],
            width: MediaQuery
                .of(context)
                .size
                .width,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Column(
                children: [
                  DropdownButton(
                    focusColor: Colors.white,
                    value: _searchBy,
                    items: [
                      DropdownMenuItem(
                        child: Text("Name"),
                        value: "name",
                      ),
                      DropdownMenuItem(
                        child: Text("NIC"),
                        value: "nic",
                      ),
                      DropdownMenuItem(
                        child: Text("Cluster"),
                        value: "cluster",
                      ),
                      DropdownMenuItem(
                        child: Text("DD"),
                        value: "dd",
                      )
                    ],
                    onChanged: (value) {
                      setState(() {
                        _searchBy = value;
                      });
                    },
                  ),
                  TextField(
                      autofocus: false,
                      decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.search, color: Colors.black45,),
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Search",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      onChanged: (value) {
                        setState(() {
                          _searchKeyword = value;
                        });
                      }
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: FutureBuilder<List<Member>>(
              future: DBProvider.db.getAllMembers(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    print("Length " + snapshot.data.length.toString());
                    return ListView.builder(
                        padding: EdgeInsets.all(8.0),
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          Member item = snapshot.data[index];
                          return ListTile(
                            title: Text(item.name),
                          );
                        },
                    );
                  } else {
                    return new Text("No Members");
                  }
                },
            ),
          )
        ],
      ),
    );
  }
}
