import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loanprocessingapp/pages/payment/print_receipt.dart';
import 'package:progress_dialog/progress_dialog.dart';

class AddPayment extends StatefulWidget {
  @override
  _AddPaymentState createState() => _AddPaymentState();
}

class _AddPaymentState extends State<AddPayment> {
  String _searchKeyword = "";
  String _searchBy = 'name';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Text("Add Payment"),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.orange[400],
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Column(
                children: [
                  DropdownButton(
                    focusColor: Colors.white,
                    value: _searchBy,
                    items: [
                      DropdownMenuItem(
                        child: Text("Name"),
                        value: "name",
                      ),
                      DropdownMenuItem(
                        child: Text("NIC"),
                        value: "nic",
                      ),
                      DropdownMenuItem(
                        child: Text("Cluster"),
                        value: "cluster",
                      ),
                      DropdownMenuItem(
                        child: Text("DDA"),
                        value: "dd_code",
                      )
                    ],
                    onChanged: (value) {
                      setState(() {
                        _searchBy = value;
                      });
                    },
                  ),
                  TextField(
                    autofocus: false,
                    decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.search,
                          color: Colors.black45,
                        ),
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Search",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5))),
                    onChanged: (value) {
                      setState(() {
                        _searchKeyword = value;
                      });
                    },
                    textCapitalization: TextCapitalization.words,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
                stream: (_searchKeyword == "")
                    ? FirebaseFirestore.instance
                        .collection("members")
                        .snapshots()
                    : FirebaseFirestore.instance
                        .collection("members")
                        .where(_searchBy,
                            isGreaterThanOrEqualTo: _searchKeyword)
                        .where(_searchBy, isLessThan: _searchKeyword + 'z')
                        .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return new Text("No Members");
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(8.0),
                      itemCount: snapshot.data.docs.length,
                      itemBuilder: (buildContext, index) =>
                          MemberRow(snapshot.data.docs[index]),
                    );
                  }
                }),
          ),
        ],
      ),
    );
  }
}

class MemberRow extends StatefulWidget {
  final DocumentSnapshot member;

  MemberRow(this.member);

  @override
  _MemberRowState createState() => _MemberRowState();
}

class _MemberRowState extends State<MemberRow> {
  final databaseReference = FirebaseFirestore.instance;

  TextEditingController amountController = new TextEditingController();

  double installment = 0;
  int loanAmount = 0;

  ProgressDialog pr;

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context);

    return Column(
      children: <Widget>[
        Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: GestureDetector(
            onTap: () {
              int amount = int.parse(widget.member.data()["loan_amount"]);
              int interest = int.parse(widget.member.data()["interest"]);
              int terms = int.parse(widget.member.data()["loanTerm"]);
              String loan_no = widget.member.data()["loan_no"];

              installment = (amount + interest) / terms;
              loanAmount = amount + interest;

              amountController = new TextEditingController(
                  text: installment.toStringAsFixed(0));

              displayDialog(context, loan_no);
            },
            child: Container(
              color: Colors.white,
              child: new ListTile(
                leading: new CircleAvatar(
                  backgroundColor: Colors.orange,
                  child: new Icon(Icons.person),
                  foregroundColor: Colors.white,
                ),
                title: Text(widget.member.data()["name"]),
                subtitle: StreamBuilder<QuerySnapshot>(
                    stream: databaseReference
                        .collection("members")
                        .doc(widget.member.id)
                        .collection("installments")
                        .orderBy("date", descending: true)
                        .limit(1)
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data.docs.length > 0) {
                        final dateToCheck =
                            snapshot.data.docs[0].data()["date"].toDate();
                        final today = DateTime(DateTime.now().year,
                            DateTime.now().month, DateTime.now().day);
                        final aDate = DateTime(dateToCheck.year,
                            dateToCheck.month, dateToCheck.day);
                        String statusText = "";
                        Color color = Colors.green;

                        if (aDate == today) {
                          statusText = "Done";
                          color = Colors.green;
                        } else {
                          statusText = "Pending";
                          color = Colors.red;
                        }

                        return Text(
                          statusText,
                          style: TextStyle(
                              color: color, fontWeight: FontWeight.bold),
                        );
                      } else {
                        return Text("No Payment Yet");
                      }
                    }),
              ),
            ),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 0.7,
          color: Colors.black12,
        )
      ],
    );
  }

  void displayDialog(BuildContext context, loan_no) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: new Text("Mark Installment"),
        content: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          child: Column(
            children: <Widget>[
              TextFormField(
                controller: amountController,
                decoration: InputDecoration(labelText: "Amount"),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter amount';
                  }
                  return null;
                },
                keyboardType: TextInputType.number,
              ),
            ],
          ),
        ),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          new FlatButton(
            child: new Text("Add"),
            onPressed: () {
              databaseReference
                  .collection("members")
                  .doc(widget.member.id)
                  .get()
                  .then((memberDetails) {
                int to_paid = int.parse(memberDetails.data()["toPaid"]);
                int amount = int.parse(amountController.text);

                if (amount > to_paid) {
                  Fluttertoast.showToast(
                      msg: "Balance exceeded. Only $to_paid to pay",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.red,
                      textColor: Colors.white,
                      fontSize: 16.0);
                } else {}
              });

              Navigator.of(context).pop();
              pr.show();

              int total = 0;
              databaseReference
                  .collection("members")
                  .doc(widget.member.id)
                  .collection("installments")
                  .where('loan_no', isEqualTo: loan_no)
                  .get()
                  .then((QuerySnapshot snapshot) {
                snapshot.docs.forEach((element) {
                  total += int.parse(element.data()["amount"]);
                });
              }).then((value) {
                int amountToPay =
                    loanAmount - total - int.parse(amountController.text);

                pr.hide();

                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PrintReceipt(
                            loanAmount.toString(),
                            installment.toStringAsFixed(0),
                            amountController.text,
                            amountToPay.toString(),
                            widget.member)));
//                    Navigator.push(context,
//                        MaterialPageRoute(builder: (context) => PrintTwo(),));
              });
            },
          ),
        ],
      ),
      barrierDismissible: false,
    );
  }
}
