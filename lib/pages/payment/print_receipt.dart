import 'dart:typed_data';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/material.dart' hide Image;
import 'package:flutter/services.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:loanprocessingapp/bluetooth/esc_pos_bluetooth.dart';
import 'package:oktoast/oktoast.dart';
import 'package:image/image.dart';
import 'package:progress_dialog/progress_dialog.dart';

class PrintReceipt extends StatefulWidget {

  final String loanAmount, installment, payment, toBePaid;
  final DocumentSnapshot member;

  PrintReceipt(
      this.loanAmount,
      this.installment,
      this.payment,
      this.toBePaid,
      this.member
      );

  @override
  _PrintReceiptState createState() => _PrintReceiptState();
}

class _PrintReceiptState extends State<PrintReceipt> {

  PrinterBluetoothManager printerManager = PrinterBluetoothManager();
  List<PrinterBluetooth> _devices = [];

  ProgressDialog pr;

  final databaseReference = FirebaseFirestore.instance;

  @override
  void initState() {
    super.initState();

    _startScanDevices();

    printerManager.scanResults.listen((devices) async {
      // print('UI: Devices found ${devices.length}');
      setState(() {
        _devices = devices;
      });
    });
  }

  void _startScanDevices() {
    setState(() {
      _devices = [];
    });
    printerManager.startScan(Duration(seconds: 1));
  }

  void _stopScanDevices() {
    printerManager.stopScan();
  }

  Future<Ticket> demoReceipt(PaperSize paper) async {
    final Ticket ticket = Ticket(paper);

   // final ByteData data = await rootBundle.load('assets/logo216.png');
   // final Uint8List bytes = data.buffer.asUint8List();
   // final Image image = decodeImage(bytes);
   //
   // ticket.image(image);

    ticket.text('EASY CREDIT LANKA LTD.',
        styles: PosStyles(align: PosAlign.center, bold: true,
          height: PosTextSize.size2,));

    ticket.text('Morawaka Branch', styles: PosStyles(align: PosAlign.center));
    ticket.text('070 5572342 / 071 9013679', styles: PosStyles(align: PosAlign.center));

    ticket.hr();

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy H:m');
    final String timestamp = formatter.format(now);

    ticket.row([
      PosColumn(text: 'Date Time', width: 4, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: timestamp, width: 8, styles: PosStyles(align: PosAlign.left)),
    ]);

    ticket.row([
      PosColumn(text: 'DDA Code', width: 4, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: widget.member.get("dd_code"), width: 8, styles: PosStyles(align: PosAlign.left)),
    ]);

    ticket.row([
      PosColumn(text: 'Cluster', width: 4, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: widget.member.get("cluster"), width: 8, styles: PosStyles(align: PosAlign.left)),
    ]);

    ticket.row([
      PosColumn(text: 'Name', width: 4, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: widget.member.get("name"), width: 8, styles: PosStyles(align: PosAlign.left)),
    ],);

    ticket.hr();
    ticket.text('PAYMENT SUMMERY', styles: PosStyles(align: PosAlign.center, bold: true));
    ticket.hr();

    ticket.row([
      PosColumn(text: 'Loan Amount', width: 8, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: widget.loanAmount+"/=", width: 4, styles: PosStyles(align: PosAlign.right)),
    ]);

    ticket.row([
      PosColumn(text: 'Installment', width: 8, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: widget.installment+"/=", width: 4, styles: PosStyles(align: PosAlign.right)),
    ]);
    ticket.row([
      PosColumn(text: 'To be Paid', width: 8, styles: PosStyles(align: PosAlign.left)),
      PosColumn(text: widget.toBePaid+"/=", width: 4, styles: PosStyles(align: PosAlign.right)),
    ]);

    ticket.hr();
    ticket.row([
      PosColumn(text: 'Payment', width: 8, styles: PosStyles(align: PosAlign.left, bold: true)),
      PosColumn(text: widget.payment+"/=", width: 4, styles: PosStyles(align: PosAlign.right, bold: true)),
    ]);
    ticket.hr(linesAfter: 1);

    ticket.text('Thank you!',
        styles: PosStyles(align: PosAlign.center, bold: true));

    // Print QR Code using native function
     ticket.qrcode(widget.member.id);

    ticket.feed(1);
    ticket.cut();
    return ticket;
  }

  Future<Ticket> testTicket(PaperSize paper) async {
    final Ticket ticket = Ticket(paper);

//    ticket.text(
//        'Regular: aA bB cC dD eE fF gG hH iI jJ kK lL mM nN oO pP qQ rR sS tT uU vV wW xX yY zZ');
//    ticket.text('Special 1: àÀ èÈ éÉ ûÛ üÜ çÇ ôÔ',
//        styles: PosStyles(codeTable: PosCodeTable.westEur));
//    ticket.text('Special 2: blåbærgrød',
//        styles: PosStyles(codeTable: PosCodeTable.westEur));

    ticket.text('Bold text', styles: PosStyles(bold: true));
    ticket.text('Reverse text', styles: PosStyles(reverse: true));
    ticket.text('Underlined text',
        styles: PosStyles(underline: true), linesAfter: 1);
    ticket.text('Align left', styles: PosStyles(align: PosAlign.left));
    ticket.text('Align center', styles: PosStyles(align: PosAlign.center));
    ticket.text('Align right',
        styles: PosStyles(align: PosAlign.right), linesAfter: 1);

    ticket.row([
      PosColumn(
        text: 'col3',
        width: 3,
        styles: PosStyles(align: PosAlign.center, underline: true),
      ),
      PosColumn(
        text: 'col6',
        width: 6,
        styles: PosStyles(align: PosAlign.center, underline: true),
      ),
      PosColumn(
        text: 'col3',
        width: 3,
        styles: PosStyles(align: PosAlign.center, underline: true),
      ),
    ]);

    ticket.text('Text size 200%',
        styles: PosStyles(
          height: PosTextSize.size2,
          width: PosTextSize.size2,
        ));

    // Print image
    final ByteData data = await rootBundle.load('assets/logo.png');
    final Uint8List bytes = data.buffer.asUint8List();
//    final Image image = decodeImage(bytes);
//    ticket.image(image);
//     Print image using alternative commands
//     ticket.imageRaster(image);
//     ticket.imageRaster(image, imageFn: PosImageFn.graphics);

    // Print barcode
    final List<int> barData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 4];
    ticket.barcode(Barcode.upcA(barData));

    // Print mixed (chinese + latin) text. Only for printers supporting Kanji mode
    // ticket.text(
    //   'hello ! 中文字 # world @ éphémère &',
    //   styles: PosStyles(codeTable: PosCodeTable.westEur),
    //   containsChinese: true,
    // );

    ticket.feed(2);

    ticket.cut();
    return ticket;
  }

  void _testPrint(PrinterBluetooth printer) async {
    printerManager.selectPrinter(printer);

    // TODO Don't forget to choose printer's paper
    const PaperSize paper = PaperSize.mm58;

    // TEST PRINT
    // final PosPrintResult res =
    // await printerManager.printTicket(await testTicket(paper));

    // DEMO RECEIPT
//    final PosPrintResult res =
//    await printerManager.printTicket(await demoReceipt(paper));



    printerManager.printTicket(await demoReceipt(paper)).then((value) {
      if (value.msg == "Success") {
        createRecord();
      }

      Fluttertoast.showToast(
          msg: value.msg,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    });

  }

  void createRecord() async {
    DateTime now = DateTime.now();

    await databaseReference
        .collection("members")
        .doc(widget.member.id)
        .collection("installments")
        .doc(now.millisecondsSinceEpoch.toString())
        .set({
      'amount': widget.payment,
      'date': now,
      'loan_no': widget.member.get("loan_no")
    });

    //create record for today income
    await FlutterSession().get("username").then((username) {
      databaseReference
          .collection("income")
          .doc(now.millisecondsSinceEpoch.toString())
          .set({
        'amount': widget.payment,
        'added_by': username,
        'loan_member': widget.member.id,
        'cluster': widget.member.get("cluster"),
        'date': now
      }).then((value) {

        databaseReference
            .collection("members")
            .doc(widget.member.id)
            .update({
          'toPaid': widget.toBePaid
        }).then((value) {
          pr.hide();

          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Text("Successfully"),
                content: new Text("Payment Completed"),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Close"),
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                  ),
                ],
              );
            },
          );
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    pr = ProgressDialog(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Print Receipt"),
      ),
      body: ListView.builder(
          itemCount: _devices.length,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                pr.show();
                _testPrint(_devices[index]);
              },
              child: Column(
                children: <Widget>[
                  Container(
                    height: 60,
                    padding: EdgeInsets.only(left: 10),
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.print),
                        SizedBox(width: 10),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(_devices[index].name ?? ''),
                              Text(_devices[index].address),
                              Text(
                                'Click to print a receipt',
                                style: TextStyle(color: Colors.grey[700]),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Divider(),
                ],
              ),
            );
          }),
      floatingActionButton: StreamBuilder<bool>(
        stream: printerManager.isScanningStream,
        initialData: false,
        builder: (c, snapshot) {
          if (snapshot.data) {
            return FloatingActionButton(
              child: Icon(Icons.stop),
              onPressed: _stopScanDevices,
              backgroundColor: Colors.red,
            );
          } else {
            return FloatingActionButton(
              child: Icon(Icons.search),
              onPressed: _startScanDevices,
            );
          }
        },
      ),
    );
  }
}
