import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:loanprocessingapp/pages/database/Database.dart';
import 'package:loanprocessingapp/pages/database/models/MemberModel.dart';
import 'package:loanprocessingapp/pages/income/collections.dart';
import 'package:loanprocessingapp/pages/income/logs.dart';
import 'package:loanprocessingapp/pages/income/my_collections.dart';
import 'package:loanprocessingapp/pages/income/total_income.dart';
import 'package:loanprocessingapp/pages/login/logout.dart';
import 'package:loanprocessingapp/pages/members/add_member.dart';
import 'package:loanprocessingapp/pages/members/clusters.dart';
import 'package:loanprocessingapp/pages/payment/add_payment.dart';
import 'package:loanprocessingapp/pages/payment/scan_qr.dart';
import 'package:loanprocessingapp/pages/profile/change_password.dart';
import 'package:loanprocessingapp/pages/users/add_user.dart';
import 'package:loanprocessingapp/pages/users/users.dart';

import 'members/members.dart';

import 'package:qrscan/qrscan.dart' as scanner;

import 'members/members_summery.dart';
import 'members/recently_added.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState() {
    super.initState();
//    getMembersList();
  }

  //add members to sqlite
  void getMembersList() {
    DBProvider.db.deleteAllMembers().then((value) {
      FirebaseFirestore.instance.collection("members").get().then((value) {
        value.docs.forEach((element) async {
          Member m = Member(
              id: element.id,
              loan_amount: element.get("loan_amount"),
              nic: element.get("nic"),
              address: element.get("address"),
              name: element.get("name"),
              cluster: element.get("cluster"),
              dd_code: element.get("dd_code"),
              interest: element.get("interest"),
              loanTerm: element.get("loanTerm"),
              mobile: element.get("mobile"),
              toPaid: element.get("toPaid")
          );

          await DBProvider.db.newMember(m);
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
        actions: [
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePassword()));
              },
            ),
          ),
          Logout()
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
          child: FutureBuilder<dynamic>(
            future: FlutterSession().get("role"),
            builder: (context, snapshot) {
              if (snapshot.data == "admin") {
                return Column(
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Member",
                                style: TextStyle(color: Colors.black45, fontSize: 18),
                              )),
                          Divider(
                            color: Colors.black38,
                            height: 20,
                            thickness: 0.3,
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 95,
                                    height: 95,
                                    child: OutlineButton(
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => AddMember()));
                                      },
                                      borderSide: BorderSide(color: Colors.lightBlue[100]),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.person_add,
                                            size: 45.0,
                                            color: Colors.lightBlue,
                                          ),
                                          Text(
                                            "Add Member",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(fontSize: 12.0),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10,),
                                  SizedBox(
                                    width: 95,
                                    height: 95,
                                    child: OutlineButton(
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => Members()));
                                      },
                                      borderSide: BorderSide(color: Colors.lightBlue[100]),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.supervised_user_circle,
                                            size: 45.0,
                                            color: Colors.lightBlue,
                                          ),
                                          Text(
                                            "Members",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(fontSize: 12.0),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10,),
                                  SizedBox(
                                    width: 95,
                                    height: 95,
                                    child: OutlineButton(
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => Clusters()));
                                      },
                                      borderSide: BorderSide(color: Colors.lightBlue[100]),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.location_on,
                                            size: 45.0,
                                            color: Colors.lightBlue,
                                          ),
                                          Text(
                                            "Clusters",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(fontSize: 12.0),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10,),
                                  SizedBox(
                                    width: 95,
                                    height: 95,
                                    child: OutlineButton(
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => RecentlyAdded()));
                                      },
                                      borderSide: BorderSide(color: Colors.lightBlue[100]),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.history,
                                            size: 45.0,
                                            color: Colors.lightBlue,
                                          ),
                                          Text(
                                            "Recently Added",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(fontSize: 12.0),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 10,),
                                  SizedBox(
                                    width: 95,
                                    height: 95,
                                    child: OutlineButton(
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => MembersSummery()));
                                      },
                                      borderSide: BorderSide(color: Colors.lightBlue[100]),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20.0),
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.book,
                                            size: 45.0,
                                            color: Colors.lightBlue,
                                          ),
                                          Text(
                                            "Summery",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(fontSize: 12.0),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Container(
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Payments",
                                style: TextStyle(color: Colors.black45, fontSize: 18),
                              )),
                          Divider(
                            color: Colors.black38,
                            height: 20,
                            thickness: 0.3,
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: 95,
                                height: 95,
                                child: OutlineButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => AddPayment()));
                                  },
                                  borderSide: BorderSide(color: Colors.orange[100]),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.payment,
                                        size: 45.0,
                                        color: Colors.orange,
                                      ),
                                      Text(
                                        "Add Payment",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(width: 10,),
                              SizedBox(
                                width: 95,
                                height: 95,
                                child: OutlineButton(
                                  onPressed: () {
                                    _scan();
                                  },
                                  borderSide: BorderSide(color: Colors.orange[100]),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.settings_overscan,
                                        size: 45.0,
                                        color: Colors.orange,
                                      ),
                                      Text(
                                        "Scan QR",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Container(
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Income",
                                style: TextStyle(color: Colors.black45, fontSize: 18),
                              )),
                          Divider(
                            color: Colors.black38,
                            height: 20,
                            thickness: 0.3,
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: 95,
                                height: 95,
                                child: OutlineButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => TotalIncome()));
                                  },
                                  borderSide: BorderSide(color: Colors.green[100]),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.monetization_on,
                                        size: 45.0,
                                        color: Colors.green,
                                      ),
                                      Text(
                                        "Income Report",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(width: 10,),
                              SizedBox(
                                width: 95,
                                height: 95,
                                child: OutlineButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Logs()));
                                  },
                                  borderSide: BorderSide(color: Colors.green[100]),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.report,
                                        size: 45.0,
                                        color: Colors.green,
                                      ),
                                      Text(
                                        "Log",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(width: 10,),
                              SizedBox(
                                width: 95,
                                height: 95,
                                child: OutlineButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Collections()));
                                  },
                                  borderSide: BorderSide(color: Colors.green[100]),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.collections_bookmark,
                                        size: 45.0,
                                        color: Colors.green,
                                      ),
                                      Text(
                                        "Collections",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Container(
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Users",
                                style: TextStyle(color: Colors.black45, fontSize: 18),
                              )),
                          Divider(
                            color: Colors.black38,
                            height: 20,
                            thickness: 0.3,
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: 95,
                                height: 95,
                                child: OutlineButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => AddUser()));
                                  },
                                  borderSide: BorderSide(color: Colors.purpleAccent[100]),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.add_to_home_screen,
                                        size: 45.0,
                                        color: Colors.purpleAccent,
                                      ),
                                      Text(
                                        "Add User",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(width: 10,),
                              SizedBox(
                                width: 95,
                                height: 95,
                                child: OutlineButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => UsersList()));
                                  },
                                  borderSide: BorderSide(color: Colors.purpleAccent[100]),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.view_list,
                                        size: 45.0,
                                        color: Colors.purpleAccent,
                                      ),
                                      Text(
                                        "Users",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                );
              } else {
                return Column(
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Member",
                                style: TextStyle(color: Colors.black45, fontSize: 18),
                              )),
                          Divider(
                            color: Colors.black38,
                            height: 20,
                            thickness: 0.3,
                          ),
                          Row(
                            children: [
                              Container(
                                width: 85,
                                child: OutlineButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => AddMember()));
                                  },
                                  borderSide: BorderSide(color: Colors.transparent),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.person_add,
                                        size: 45.0,
                                        color: Colors.lightBlue,
                                      ),
                                      Text(
                                        "Add Member",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                width: 85,
                                child: OutlineButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Members()));
                                  },
                                  borderSide: BorderSide(color: Colors.transparent),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.supervised_user_circle,
                                        size: 45.0,
                                        color: Colors.lightBlue,
                                      ),
                                      Text(
                                        "Members",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                width: 85,
                                child: OutlineButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Clusters()));
                                  },
                                  borderSide: BorderSide(color: Colors.transparent),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.location_on,
                                        size: 45.0,
                                        color: Colors.lightBlue,
                                      ),
                                      Text(
                                        "Clusters",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Container(
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Payments",
                                style: TextStyle(color: Colors.black45, fontSize: 18),
                              )),
                          Divider(
                            color: Colors.black38,
                            height: 20,
                            thickness: 0.3,
                          ),
                          Row(
                            children: [
                              Container(
                                width: 85,
                                child: OutlineButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => AddPayment()));
                                  },
                                  borderSide: BorderSide(color: Colors.transparent),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.payment,
                                        size: 45.0,
                                        color: Colors.orange,
                                      ),
                                      Text(
                                        "Add Payment",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Container(
                      child: Column(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Income",
                                style: TextStyle(color: Colors.black45, fontSize: 18),
                              )),
                          Divider(
                            color: Colors.black38,
                            height: 20,
                            thickness: 0.3,
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: 95,
                                height: 95,
                                child: OutlineButton(
                                  onPressed: () {
                                    FlutterSession().get("username")
                                        .then((value) {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => MyCollections(value)));
                                    });
                                  },
                                  borderSide: BorderSide(color: Colors.green[100]),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.collections_bookmark,
                                        size: 45.0,
                                        color: Colors.green,
                                      ),
                                      Text(
                                        "Collections",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12.0),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                );
              }

            }
          ),
        ),
      ),
    );
  }

  Future _scan() async {
    String barcode = await scanner.scan();
    Navigator.push(context, MaterialPageRoute(builder: (context) => ScanQr(barcode)));
  }
}
