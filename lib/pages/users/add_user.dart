import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loanprocessingapp/pages/home_page.dart';
import 'package:progress_dialog/progress_dialog.dart';

class AddUser extends StatefulWidget {
  @override
  _AddUserState createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  var usernameController = new TextEditingController();
  var passwordController = new TextEditingController(text: "Abcd1234");
  var cpasswordController = new TextEditingController(text: "Abcd1234");

  String currentSelectedValue = 'employee';

  ProgressDialog pr;

  @override
  Widget build(BuildContext context) {

    pr = ProgressDialog(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Add User"),
        backgroundColor: Colors.purpleAccent,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _form,
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: usernameController,
                  decoration: InputDecoration(
                      labelText: "Username",
                      hintText: "Username",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5))),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Username cannot be empty';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(left:8.0, right: 8.0),
                  child: Text(
                      "Automatically set default password as 'Abcd1234'. Change it as you wish or user can change it after login."),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: passwordController,
                  decoration: InputDecoration(
                      labelText: "New Password",
                      hintText: "New Password",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5))),
                  obscureText: true,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Password cannot be empty';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: cpasswordController,
                  decoration: InputDecoration(
                      labelText: "Confirm Password",
                      hintText: "Confirm Password",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5))),
                  obscureText: true,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Confirm password cannot be empty';
                    } else if (value != passwordController.text) {
                      return 'Passwords not matched';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                FormField<String>(
                  builder: (FormFieldState<String> state) {
                    return InputDecorator(
                      decoration: InputDecoration(
                          errorStyle: TextStyle(color: Colors.redAccent, fontSize: 16.0),
                          labelText: "Role",
                          hintText: 'Please select role',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      isEmpty: currentSelectedValue == '',
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          value: currentSelectedValue,
                          isDense: true,
                          onChanged: (String newValue) {
                            setState(() {
                              currentSelectedValue = newValue;
                              state.didChange(newValue);
                            });
                          },
                          items: [
                            DropdownMenuItem(
                              child: Text("Employee"),
                              value: "employee",
                            ),
                            DropdownMenuItem(
                              child: Text("Admin"),
                              value: "admin",
                            ),
                          ]
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: 40,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, right: 20),
                  child: ButtonTheme(
                    buttonColor: Colors.purpleAccent,
                    minWidth: MediaQuery.of(context).size.width,
                    height: 50.0,
                    child: RaisedButton(
                      onPressed: () {
                        if (_form.currentState.validate()) {
                          pr.show();
                          createRecord();
                        }
                      },
                      child: Text(
                        "SUBMIT",
                        style: TextStyle(color: Colors.white, fontSize: 16.0),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void createRecord() {
    FirebaseFirestore.instance.collection("users").doc().set({
      'username': usernameController.text,
      'password': passwordController.text,
      'role': currentSelectedValue
    }).then((value) {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => HomePage()));
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Successfully"),
            content: new Text("User added successfully"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    });
  }
}
