import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class UsersList extends StatefulWidget {
  @override
  _UsersListState createState() => _UsersListState();
}

class _UsersListState extends State<UsersList> {
  String _searchKeyword = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Users List"),
        backgroundColor: Colors.purpleAccent,
      ),
      body: Column(
        children: [
          Container(
            color: Colors.purpleAccent[400],
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Column(
                children: [
                  TextField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.search, color: Colors.black45,),
                          fillColor: Colors.white,
                          filled: true,
                          hintText: "Search",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5))),
                      onChanged: (value) {
                        setState(() {
                          _searchKeyword = value;
                        });
                      }
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
                stream: (_searchKeyword == "") ? FirebaseFirestore.instance.collection("users").snapshots() :
                FirebaseFirestore.instance.collection("users").where("username", isEqualTo: _searchKeyword).snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return new Column(
                      children: [
                        Icon(Icons.search, size: 80, color: Colors.black26,),
                        Center(child: Text("No users found yet!", style: TextStyle(fontSize: 16),)),
                      ],
                    );
                  } else {
                    return ListView.builder(
                      padding: EdgeInsets.all(8.0),
                      itemCount: snapshot.data.docs.length,
                      itemBuilder: (buildContext, index) =>
                          MemberRow(snapshot.data.docs[index]),
                    );
                  }
                }),
          ),
        ],
      )
    );
  }
}

class MemberRow extends StatefulWidget {

  final DocumentSnapshot member;

  MemberRow(this.member);

  @override
  _MemberRowState createState() => _MemberRowState();
}

class _MemberRowState extends State<MemberRow> {

  final databaseReference = FirebaseFirestore.instance;

  TextEditingController amountController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Slidable(
          actionPane: SlidableDrawerActionPane(),
          actionExtentRatio: 0.25,
          child: GestureDetector(
            onTap: () {
              displayResetPasswordDialog();
            },
            child: Container(
              color: Colors.white,
              child: new ListTile(
                leading: new CircleAvatar(
                  backgroundColor: Colors.purpleAccent,
                  child: new Icon(Icons.person),
                  foregroundColor: Colors.white,
                ),
                title: Text(widget.member.data()["username"]),
                subtitle: Text("Role: " + widget.member.data()["role"]),
              ),
            ),
          ),
          secondaryActions: <Widget>[
            IconSlideAction(
              caption: 'Delete',
              color: Colors.red,
              icon: Icons.delete,
              onTap: () => {
                if (widget.member.data()["role"] == "admin") {
                  displayCantDeleteDialog(context)
                } else {
                  displayDeleteDialog(context)
                }
              },
            ),
          ],
        ),
        Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          height: 0.7,
          color: Colors.black12,
        )
      ],
    );
  }

  void displayDeleteDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) =>
          AlertDialog(
            title: new Text("Delete Member"),
            content: Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.07,
              child: Column(
                children: <Widget>[
                  Text("Are you sure, you need to delete this member from the system?"),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("No"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new FlatButton(
                child: new Text("Yes"),
                onPressed: () {
                  Navigator.of(context).pop();
                  deleteRecord();
                },
              ),
            ],
          ),
      barrierDismissible: false,
    );
  }

  void displayCantDeleteDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) =>
          AlertDialog(
            title: new Text("Admin Account"),
            content: Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.1,
              child: Column(
                children: <Widget>[
                  Text("This account is admin account. So you can't delete this account from the system."),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
    );
  }

  void deleteRecord() async {
    await databaseReference
        .collection("users")
        .doc(widget.member.id)
        .delete().then((value) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Successfully"),
            content: new Text("User deleted successfully"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    });
  }

  void displayResetPasswordDialog() {
    showDialog(
      context: context,
      builder: (_) =>
          AlertDialog(
            title: new Text("Reset Password"),
            content: Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.1,
              child: Column(
                children: <Widget>[
                  Text("Password will be set as 'Abcd1234"),
                ],
              ),
            ),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new FlatButton(
                child: new Text("Ok"),
                onPressed: () {
                  resetPassword();
                },
              ),
            ],
          ),
    );
  }

  void resetPassword() {
    databaseReference.collection("users").doc(widget.member.id).update({
      'password': 'Abcd1234'
    }).then((value) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Successfully"),
            content: new Text("Password reset successfully"),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    });
  }
}
