import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Destination {
  Destination(this.title, this.icon);
  final String title;
  final IconData icon;
}